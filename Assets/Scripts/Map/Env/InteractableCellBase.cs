using System;
using Core;
using UnityEngine;

namespace Map.Env
{
    public abstract class PlayableCellBase : MonoBehaviour, IPlayable
    {
        public enum CellType { Start, Fill, End }
        
        public bool IsPlaying => _isPlaying;

        public event Action OnPlay;
        public event Action OnStop;
        [SerializeField] protected SpriteRenderer _renderer;
        
        [SerializeField] protected Sprite _StartSprite;
        [SerializeField] protected Sprite _FillSprite;
        [SerializeField] protected Sprite _EndSprite;


        private bool _isPlaying = false;
        private CellType _type = CellType.Fill;


        public virtual void SetCellType(CellType type)
        {
            _type = type;
        }

        public void Play()
        {
            if (IsPlaying) { return; }

            _renderer.sprite = _type == CellType.Start ? _StartSprite
                : _type == CellType.End ? _EndSprite : _FillSprite;

            Played();

            _isPlaying = true;
            gameObject.SetActive(true);
            OnPlay?.Invoke();
        }
        public void Stop()
        {
            if (!IsPlaying) { return; }

            Stoped();

            gameObject.SetActive(false);
            _isPlaying = false;
            OnStop?.Invoke();
        }

        protected abstract void Played();
        protected abstract void Stoped();
    }
}