using UnityEngine;

namespace Map.Env
{
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class BridgeCell : PlayableCellBase
    {
        [SerializeField] protected LayerMask _targetLayerMask;
        private Rigidbody2D _rb;

        private bool _isFall = false;
        private float _minFallY = -10f;


        private void Awake()
        {
            _rb = GetComponent<Rigidbody2D>();

            SetFallEnabled(false);

            SetMinY();
        }

        protected override void Played()
        {
            SetFallEnabled(false);
        }
        protected override void Stoped()
        {
            SetFallEnabled(false);
        }

        private void FixedUpdate()
        {
            if (_isFall)
            {
                if (transform.position.y < _minFallY)
                {
                    Stop();
                }
            }
        }

        private void SetFallEnabled(bool value)
        {
            _isFall = value;

            _rb.bodyType = _isFall ? RigidbodyType2D.Dynamic : RigidbodyType2D.Static;            
        }

        private void SetMinY()
        {
            var cam = Camera.main;
            if (cam && cam.orthographic)
            {
                _minFallY = cam.transform.position.y - cam.orthographicSize * 1.5f;
            }
        }

        protected bool CheckLayer(int layer)
        {
            return _targetLayerMask == (_targetLayerMask | (1 << layer));
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            if (!CheckLayer(collision.gameObject.layer)) { return; }

            SetFallEnabled(true);
        }
    }
}