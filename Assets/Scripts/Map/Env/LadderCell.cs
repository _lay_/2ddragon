using UnityEngine;

namespace Map.Env
{

    [RequireComponent(typeof(Collider2D))]
    public class LadderCell : PlayableCellBase
    {
        protected override void Played()
        {
            
        }

        protected override void Stoped()
        {
            
        }
    }
}