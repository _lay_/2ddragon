using Units;
using UnityEngine;

namespace Map.Env
{
    [RequireComponent(typeof(Collider2D))]
    public class MovableWaitCell : PlayableCellBase
    {
        private enum MoveDir { Vertical, Horizontal }

        [SerializeField] private MoveDir _moveDir = MoveDir.Vertical;
        [SerializeField] private float _moveOffset = 1f;
        [SerializeField] private float _moveSmoothTime = .2f;
        [SerializeField] private float _waitTime = 2f;

        private float _minX;
        private float _maxX;
        private float _minY;
        private float _maxY;

        private bool _needMove;
        private Vector3 _targetPos;
        private Vector3 _velocity;
        private float _waitTimer;


        protected override void Played()
        {
            _minX = transform.position.x - _moveOffset;
            _maxX = transform.position.x + _moveOffset;
            _minY = transform.position.y - _moveOffset;
            _maxY = transform.position.y + _moveOffset;

            if (_moveDir ==  MoveDir.Horizontal)
            {
                _targetPos = new Vector3(_maxX, transform.position.y, transform.position.z);
            }
            else if (_moveDir ==  MoveDir.Vertical)
            {
                _targetPos = new Vector3(transform.position.x, _maxY, transform.position.z);
            }

            _velocity = Vector3.zero;

            _needMove = true;
        }

        protected override void Stoped()
        {
            
        }

        private void Update()
        {
            if (!IsPlaying) { return; }

            if (_needMove)
            {
                transform.position = Vector3.SmoothDamp(transform.position, _targetPos, ref _velocity, _moveSmoothTime);
            }
        }

        private void FixedUpdate()
        {
            if (!IsPlaying) { return; }

            if (_needMove)
            {
                if (TryUpdateTargetPos())
                {
                    _needMove = false;
                    _waitTimer = _waitTime;
                }
            }
            else
            {
                if (_waitTimer > 0)
                {
                    _waitTimer -= Time.fixedDeltaTime;
                }
                else
                {
                    _velocity = Vector3.zero;
                    _needMove = true;
                }
            }
        }

        private bool TryUpdateTargetPos()
        {
            if (_moveDir ==  MoveDir.Horizontal)
            {
                if (transform.position.x+.1f >= _maxX)
                {
                    _targetPos = new Vector3(_minX, transform.position.y, transform.position.z);
                    return true;
                }
                else if (transform.position.x-.1f <= _minX)
                {
                    _targetPos = new Vector3(_maxX, transform.position.y, transform.position.z);
                    return true;
                }
            }
            else if (_moveDir ==  MoveDir.Vertical)
            {
                if (transform.position.y+.1f >= _maxY)
                {
                    _targetPos = new Vector3(transform.position.x, _minY, transform.position.z);
                    return true;
                }
                else if (transform.position.y-.1f <= _minY)
                {
                    _targetPos = new Vector3(transform.position.x, _maxY, transform.position.z);
                    return true;
                }
            }

            return false;
        }
    }
}