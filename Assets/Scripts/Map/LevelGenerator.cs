using System;
using UnityEngine;
using Core.Map;
using Units;
using Game;

namespace Map
{
    public class LevelGenerator : MonoBehaviour
    {
        public LayeredTileMap TileMap => _tileMap;

        [Space()]
        [SerializeField] private UnityEngine.Tilemaps.TileBase _zoneBlockTile;
        [SerializeField] private int _startZoneWidth = 6;
        [SerializeField] private int _safeZoneWidth = 15;
        [SerializeField] private int _safeZoneEach = 5;
        [Space()]
        [SerializeField, Min(0)] protected int _startHeight = 5;
        [SerializeField, Min(1)] private int _generateHeight = 8;
        [SerializeField, Min(10)] private int _generateWidth = 50;
        [Space()]
        [SerializeField] private LayeredTileMap _tileMap;
        [Space()]
        [SerializeField] private GeneratorRuleContainer _generateRule;

        [SerializeField] private int _generateBoundOffset = -12;
#if UNITY_EDITOR
        [Header("Editor Only")]
        [SerializeField] private bool _testGenerateOnStart = false;
        [SerializeField] private int _testGenerateCount = 20;
#endif

        private int _nextStartX;
        private int _startY;

        private int _nextBlockDistance;
        private int _generatedBlocksCount;
        private int _nextZoneGeneratedAtBlock;
        private bool _blockGanerateEnabled;

        private MapModel _prevMap;
        private MapModel _curMap;


        public void StartLevel()
        {
            ClearLevel();
            _generateRule.SetTileMapAll(_tileMap);

            _prevMap = new MapModel(0, _generateHeight);
            _curMap = new MapModel(0, _generateHeight);

            _nextStartX = 0;
            _startY = _startHeight;
            _nextBlockDistance = 0;
            _generatedBlocksCount = 0;
            _nextZoneGeneratedAtBlock = _safeZoneEach + 1;
            _blockGanerateEnabled = false;

            GenerateStartZone();

            StartBlocksGenerating();
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                GenerateNext();
            }
#endif
        }

        private void FixedUpdate()
        {
            if (_blockGanerateEnabled)
            {
                if (GameInstance.Instance.GameMode.Distance > _nextBlockDistance)
                {
                    GenerateNext();
                }
            }
        }

#region ZonesGenerate

        private void GenerateStartZone()
        {
            var tile = _zoneBlockTile;
            int endX = _nextStartX + _startZoneWidth;
            int y = _startY;

            for (int x = _nextStartX; x < endX; x++)
            {
                _tileMap.AddTileTo(MapLayers.Main, x, y, _zoneBlockTile);
                _tileMap.AddTileTo(MapLayers.Main, x, y-1, _zoneBlockTile);
                if (x > _nextStartX && x < endX-1)
                {
                    _tileMap.AddTileTo(MapLayers.Main, x, y-2, _zoneBlockTile);
                }
            }

            var player = GameInstance.Instance.GameMode.Player;
            player.transform.position = _tileMap.GetDownWorldPoint((int)((endX - _nextStartX) * .5f), y+1);

            _nextBlockDistance = _nextStartX - 1;

            ChangeMaps();
            _curMap = new MapModel(_startZoneWidth+1, _generateHeight);
            CalcStartX();
        }

        private void GenerateSafeZone()
        {
            var tile = _zoneBlockTile;
            int endX = _nextStartX + _safeZoneWidth;

            
            for (int x = _nextStartX; x < endX; x++)
            {
                for (int y = _startY; y > 0 && y > _startY-2; y--)
                {
                    _tileMap.AddTileTo(MapLayers.Main, x, y, _zoneBlockTile);
                }
            }
            
            _nextBlockDistance = endX  + _generateBoundOffset;

            ChangeMaps();
            _curMap = new MapModel(_safeZoneWidth, _generateHeight);
            CalcStartX();
        }

#endregion

        private void StartBlocksGenerating()
        {
#if UNITY_EDITOR
            if (_testGenerateOnStart)
            {
                for (int i = 0; i < _testGenerateCount; i++)
                {
                    GenerateNext();
                }
            }
            _testGenerateOnStart = false;
#endif

            _blockGanerateEnabled = true;
        }

        private void StopBlocksGenerating()
        {
            _blockGanerateEnabled = false;
        }

        private void GenerateNext()
        {
            if (_generatedBlocksCount + 1 == _nextZoneGeneratedAtBlock)
            {
                GenerateSafeZone();
                _nextZoneGeneratedAtBlock += _safeZoneEach + 1;
            }
            else
            {
                GenerateBlock();
            }
        }

        private void GenerateBlock()
        {
#if UNITY_EDITOR
            _generateRule.SetTileMapAll(_tileMap);
#endif

            ChangeMaps();

            _generateRule.GenerateAndWriteAll(_curMap, _startY, _nextStartX);

            CalcStartY();
            CalcStartX();

            _nextBlockDistance = _nextStartX + _generateBoundOffset;
            _generatedBlocksCount++;
        }

        private void CalcStartY()
        {     
            int newY = -1;
            for (int x = _curMap.Width-1; x >= 0; x--)
            {
                for (int y = 0; y < _curMap.Height; y++)
                {
                    if (_curMap.Get(x, y) == MapModelElemType.Ground)
                    {
                        newY = y;
                        break;
                    }
                }
                if (newY != -1) { break; }
            }
            _startY = newY == -1 ? _startHeight : newY;
        }
        private void CalcStartX()
        {
            _nextStartX += _curMap.Width;
        }

        private MapModel GetNewMap()
        {
            return new MapModel(_generateWidth, _generateHeight);
        }

        private void ClearPrev()
        {
            int end = _nextStartX - _curMap.Width;
            int start = end - _prevMap.Width;
            Debug.Log($"Clear {start} {end}");

            _tileMap.ClearMaps(start, end, _generateHeight);
            Array.ForEach(_generateRule.Writers, w => { var ww = w as IPooledEnvWriter; ww?.ClearEnvItems(end); });
        }

        private void ClearLevel()
        {
            _tileMap.ResetMaps();
            Array.ForEach(_generateRule.Writers, w => { var ww = w as IPooledEnvWriter; ww?.ClearAllEnvItems(); });
        }

        private void ChangeMaps()
        {
#if UNITY_EDITOR
            if (!_testGenerateOnStart)
            {
                ClearPrev();
            }
#else
            ClearPrev();
#endif

            _prevMap = _curMap;
            _curMap = GetNewMap();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<Player>() == null) { return; }
            GenerateNext();
        }
    }
}
