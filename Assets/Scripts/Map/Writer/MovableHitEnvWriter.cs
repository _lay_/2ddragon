using Core.Map;
using Map.Env;

namespace Map
{
    public class MovableHitEnvWriter : PooledMapWriterBase<MovableWaitCell>
    {
         protected override void PreWrite()
        {
            base.PreWrite();
        }

        protected override void Write()
        {
            ForEachElement((x, y) =>
            {
                if (_model.Get(x, y) == MapModelElemType.MovableHit)
                {
                    MovableWaitCell item = GetEnvItem();
                
                    item.transform.position = _tileMap.GetCenterWorldPoint(_startX + x, y);
                    item.SetCellType(PlayableCellBase.CellType.Fill);
                    item.Play();
                }
            });
        }
    }
}
