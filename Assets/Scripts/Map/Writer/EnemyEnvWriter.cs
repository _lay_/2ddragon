using Core.Map;
using Units;

namespace Map
{
    public class EnemyEnvWriter : PooledMapWriterBase<Enemy>
    {
        protected override void PostWrite()
        {
            base.PostWrite();
        }

        protected override void PreWrite()
        {
            base.PreWrite();
        }

        protected override void Write()
        {
            ForEachElement((x, y) =>
            {
                var item = _model.Get(x, y);
                if (item == MapModelElemType.Enemy)
                {
                    Enemy enemy = GetEnvItem();
                
                    enemy.transform.position = _tileMap.GetDownWorldPoint(_startX + x, y);
                    enemy.Play();
                }
            });
        }
    }
}
