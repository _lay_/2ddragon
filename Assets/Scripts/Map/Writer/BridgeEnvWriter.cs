using System.Collections.Generic;
using Core.Map;
using Map.Env;

namespace Map
{
    public class BridgeEnvWriter : PooledMapWriterBase<BridgeCell>
    {
        private struct Bridge
        {
            public int startX;
            public int endX;
            public int y;

            public Bridge(int startX, int endX, int y)
            {
                this.startX = startX;
                this.endX = endX;
                this.y = y;
            }
        }

        protected override void PreWrite()
        {
            base.PreWrite();
        }

        protected override void Write()
        {
            var items = GetBridges();
            
            foreach (var item in items)
            {
                for (var x = item.startX+1; x <= item.endX-1; x++)
                {
                    SetCell(PlayableCellBase.CellType.Fill, x, item.y);
                }
                
                SetCell(PlayableCellBase.CellType.Start, item.startX, item.y);
                SetCell(PlayableCellBase.CellType.End, item.endX, item.y);
            }
        }

        private void SetCell(PlayableCellBase.CellType type, int x, int y)
        {
            BridgeCell cell = GetEnvItem();
                
            cell.transform.position = _tileMap.GetCenterWorldPoint(_startX + x, y);
            cell.SetCellType(type);
            cell.Play();
        }

        private Bridge[] GetBridges()
        {
            List<Bridge> founded = new List<Bridge>();

            int h = _model.Height;
            int w = _model.Width;

            var elemType = MapModelElemType.Bridge;

            int startX = -1;
            int startY = 0;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    if (startX != -1)
                    {
                        if (_model.Get(x, y) == elemType)
                        {
                            continue; 
                        }
                        else
                        {
                            founded.Add(new Bridge(startX, x-1, startY));
                            startX = -1;
                        }
                    }
                    else
                    {
                        if (_model.Get(x, y) == elemType && (x == 0 || _model.Get(x-1, y) != elemType))
                        {
                            startX = x;
                            startY = y;
                        }
                    }
                }
                
                if (startX != -1)
                {
                    founded.Add(new Bridge(startX, w-1, startY));
                    startX = -1;
                }
            }

            return founded.ToArray();
        }
    }
}
