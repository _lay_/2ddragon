using UnityEngine;
using Core.Map;
using UnityEngine.Tilemaps;

namespace Map
{
    public class FillDownTileMapWriter : TileMapWriterBase
    {
        protected override void Write()
        {
            int w = _model.Width;
            int h = _model.Height;

            for (int x = 0; x < w; x++)
            {
                bool fill = false;

                for (int y = h-1; y >= 0; y--)
                {
                    var placed = _model.Get(x, y);
    
                    if (placed == _targetTileType)
                    {
                        fill = true;
                    }

                    if (fill && placed != _targetTileType)
                    {
                        _tileMap.AddTileTo(_targetMapLayer, x + _startX, y, _regularTile);
                    }
                }
            }
        }
    }
}

