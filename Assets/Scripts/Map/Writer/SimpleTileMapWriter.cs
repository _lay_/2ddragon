using UnityEngine;
using Core.Map;

namespace Map
{
    public class SimpleTileMapWriter : TileMapWriterBase
    {
        protected override void Write()
        {
            WriteNotNoneAt(_targetMapLayer, _regularTile);
        }
    }
}

