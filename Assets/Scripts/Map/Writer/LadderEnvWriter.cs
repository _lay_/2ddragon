using System.Collections.Generic;
using Core.Map;
using Map.Env;

namespace Map
{
    public class LadderEnvWriter : PooledMapWriterBase<LadderCell>
    {
        private struct Ladder
        {
            public int startY;
            public int endY;
            public int x;

            public Ladder(int startY, int endY, int x)
            {
                this.startY = startY;
                this.endY = endY;
                this.x = x;
            }
        }

        protected override void PreWrite()
        {
            base.PreWrite();
        }

        protected override void Write()
        {
            var items = GetLadders();
            
            foreach (var item in items)
            {
                for (var y = item.startY+1; y <= item.endY; y++)
                {
                    SetCell(PlayableCellBase.CellType.Fill, item.x, y);
                }
                
                SetCell(PlayableCellBase.CellType.Start, item.x, item.startY);
                SetCell(PlayableCellBase.CellType.End, item.x, item.endY+1);
            }
        }

        private void SetCell(PlayableCellBase.CellType type, int x, int y)
        {
            LadderCell cell = GetEnvItem();
                
            cell.transform.position = _tileMap.GetCenterWorldPoint(_startX + x, y);
            cell.SetCellType(type);
            cell.Play();
        }

        private Ladder[] GetLadders()
        {
            List<Ladder> founded = new List<Ladder>();

            int h = _model.Height;
            int w = _model.Width;

            var elemType = MapModelElemType.Climb;

            int startX = 0;
            int startY = -1;
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    if (startY != -1)
                    {
                        if (_model.Get(x, y) == elemType)
                        {
                            continue; 
                        }
                        else
                        {
                            founded.Add(new Ladder(startY, y-1, startX));
                            startY = -1;
                        }
                    }
                    else
                    {
                        if (_model.Get(x, y) == elemType && (y == 0 || _model.Get(x, y-1) != elemType))
                        {
                            startX = x;
                            startY = y;
                        }
                    }
                }
                
                if (startY != -1)
                {
                    founded.Add(new Ladder(startY, h-1, startX));
                    startY = -1;
                }
            }

            return founded.ToArray();
        }
    }
}
