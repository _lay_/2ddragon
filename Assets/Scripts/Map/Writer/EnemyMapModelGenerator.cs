using System.Collections.Generic;
using UnityEngine;
using Core.Map;
using Core.Map.Rhythm;

namespace Map
{
    public class EnemyMapModelGenerator : MapModelGeneratorBase
    {
        [SerializeField] private int _minNeededWidth = 5;

        private struct Platform
        {
            public int x;
            public int y;
            public int width;

            public Platform(int x=0, int y=0, int width=0)
            {
                this.x = x;
                this.y = y;
                this.width = width;
            }
        }


        protected override void PostGenerate()
        {
            base.PostGenerate();
        }

        protected override void PreGenerate()
        {
            base.PreGenerate();
        }

        protected override void Generate()
        {
            for (int i = 0; i < _rhythm.Actions.Length; i++)
            {
                var act = _rhythm.Actions[i];
                
                if (act.Type != ActionType.Fight)
                {
                    continue;
                }

                int startX = GetRhythmMapPoint(act.StartTime - act.Duration);
                int endX = GetRhythmMapPoint(act.StartTime + act.Duration);

                var platforms = GetPlatforms(startX, endX);

                foreach (var pl in platforms)
                {
                    if (pl.width > _minNeededWidth)
                    {
                        _map.Set(Mathf.FloorToInt(pl.x + pl.width * .5f), pl.y + 1, MapModelElemType.Enemy);
                    }
                }
            }
        }

        private Platform[] GetPlatforms(int startX, int endX)
        {
            List<Platform> founded = new List<Platform>();

            var tile = MapModelElemType.Ground;

            Platform current = new Platform();
            for (int y = 0; y < _height; y++)
            {
                for (int x = startX; x < endX; x++)
                {
                    if (current.width != 0)
                    {
                        if (_map.Get(x, y) == tile)
                        {
                            current.width++;
                            continue; 
                        }
                        else
                        {
                            founded.Add(current);
                            current = new Platform();
                        }
                    }
                    else
                    {
                        if (_map.Get(x, y) == tile && (x == 0 || _map.Get(x-1, y) != tile))
                        {
                            current.x = x;
                            current.y = y;
                            current.width = 1;
                        }
                    }
                }
                if (current.width != 0)
                {
                    founded.Add(current);
                    current = new Platform();
                }
            }

            return founded.ToArray();
        }
    }
}

