using UnityEngine;
using Core.Map;
using Core.Map.Rhythm;

namespace Map
{
    public class TestGeometryMapModelGenerator : MapModelGeneratorBase
    {
        private int curHeight;
        private Action _curAction;


        protected override void PreGenerate()
        {
            if (_width > 1000)
            {
                Debug.LogWarning(nameof(TestGeometryMapModelGenerator) + " maw.w maybe to big");
            }

            curHeight = _startPathHeight;
        }

        protected override void PostGenerate()
        {
            
        }

        protected override void Generate()
        {
            int startX = 0;

            for (int i = 0; i < _rhythm.Actions.Length; i++)
            {
                var act = _rhythm.Actions[i];
                _curAction = _rhythm.Actions[i];
                int endX = GetActionEndMapPoint(i);
                
                if (act.Type == ActionType.Jump)
                {
                    GenerateChangeWay(startX, endX);
                }
                else if (act.Type == ActionType.TwoWay)
                {
                    GenerateTwoWay(startX, endX);
                }
                else if (act.Type == ActionType.Run)
                {
                    GenerateRun(startX, endX);
                }
                else if (act.Type == ActionType.Wait)
                {
                    GenerateWait(startX, endX);
                }
                else
                {
                    GenerateMove(startX, endX);
                }

                startX = endX;
            }
        }

        private int GetPossibleYDir(int neededOffset=1)
        {           
            if (curHeight <= neededOffset+1)
            {
                return 1;
            }
            if (curHeight+curHeight >= _height || curHeight > _startPathHeight+2)
            {
                return -1;
            }

            return Random.Range(0, 2) == 0 ? -1 : 1;
        }

        private void GenerateMove(int startX, int endX)
        {
            for (int x = startX; x < endX; x++)
            {
                _map.Set(x, curHeight, MapModelElemType.Ground);
            }
        }

        private void GenerateChangeWay(int startX, int endX)
        {
            int targetOffset = (endX - startX > 3 ? 2 : 1);
            int targetOffsetDir = GetPossibleYDir(targetOffset);

            for (int x = startX; x < endX; x++)
            {
                if (targetOffset > 0)
                {
                    curHeight = curHeight + targetOffsetDir;
                    targetOffset--;
                }
                _map.Set(x, curHeight, MapModelElemType.Ground);
            }
        }

        private void GenerateTwoWay(int startX, int endX)
        {
            int len = endX - startX;
            int targetWayOffset = len > 2 ? 3 : 2;
            int offsetDir = GetPossibleYDir(targetWayOffset);

            int curOffset = len < 3
                ? Random.Range(0, targetWayOffset)
                : targetWayOffset;

            for (int x = startX; x < endX; x++)
            {
                if (targetWayOffset > curOffset)
                {
                    curOffset++;
                }

                _map.Set(x, curHeight + curOffset * offsetDir, MapModelElemType.Ground);
                _map.Set(x, curHeight, MapModelElemType.Ground);
               
            }
        }

        private void GenerateRun(int startX, int endX)
        {
            for (int x = startX; x < endX; x++)
            {
                _map.Set(x, curHeight, MapModelElemType.Bridge);
            }
        }

        private void GenerateWait(int startX, int endX)
        {
            int dist = endX - startX;
            if (dist < 3)
            {
                GenerateMove(startX, endX);
                return;
            }
            
            int offsetY = 3;
            int dirY = GetPossibleYDir(offsetY);
            int targetY = curHeight + offsetY * dirY;

            _map.Set(startX, targetY, MapModelElemType.Ground);
            _map.Set(startX, curHeight, MapModelElemType.Ground);
            for (int x = startX; x < endX; x++)
            {
                _map.Set(x, targetY, MapModelElemType.Ground);
            }

            int y = curHeight + dirY;
            while (y != targetY)
            {
                _map.Set(startX, y, MapModelElemType.Climb);
                y += dirY;
            }

            if (dist > 4)
            {
                _map.Set(startX + (int)(dist * .7f), targetY + 2, MapModelElemType.MovableHit);
            }

            curHeight = targetY;
        }
    }
}
