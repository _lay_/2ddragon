using UnityEngine;
using Core.Map;

namespace Map.Generate
{
    public class MainPathMapModelGenerator : MapModelGeneratorBase
    {
        [SerializeField] private AnimationCurve _curve =
            new AnimationCurve(new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1));
        
        protected override void Generate()
        {
            int curHeight = _startPathHeight;

            for (int i = 0; i < _width; i++)
            {
                float cr = _curve.Evaluate((float)i / _width);
                MapModelElemType tile = MapModelElemType.Ground;

                if (cr < 0f)
                {
                    continue;
                }
                if (cr > 1f)
                {
                    cr -= 1;
                    tile = MapModelElemType.Platform;
                }

                float r = Random.Range(0f, 1f);

                if (r < cr)
                {
                    curHeight += Mathf.CeilToInt((Random.Range(-curHeight, _height - curHeight) * r));
                }
                
                _map.Set(i, curHeight, tile);
            }
        }
    }
}

