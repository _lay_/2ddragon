using UnityEngine;
using Core.Map;

namespace Map.Generate
{
    public class TestMapModelGenerator : MapModelGeneratorBase
    {
        [SerializeField, Range(0f, 1f)] protected float _heightRandomChance = 0.3f;

        protected override void Generate()
        {
            int ofs = 2;

            for (int i = 0; i < _width; i++)
            {
                int y = Random.Range(0f, 1f) > _heightRandomChance
                    ? _startPathHeight
                    : Random.Range(_startPathHeight-ofs, _startPathHeight+ofs);
                
                _map.Set(i, y, MapModelElemType.Ground); 
            }
        }
    }
}

