using UnityEngine;
using Core.Map;

namespace Map.Generate
{
    public class RandomWalkMapModelGenerator : MapModelGeneratorBase
    {
        [SerializeField] private int _minSectionWidth = 2;
        [SerializeField] private int _minDownOffset = 1;
        [SerializeField] private int _maxUpOffset = 1;

        protected override void Generate()
        {
            int lastHeight = _startPathHeight;
            
            int nextMove = 0;
            int sectionWidth = 0;

            MapModelElemType tile = MapModelElemType.Ground;

            for (int x = 0; x < _width; x++) 
            {
                nextMove = Random.Range(0, 2);

                if (sectionWidth > _minSectionWidth)
                { 
                    if (nextMove == 0 && lastHeight > _minDownOffset+1) 
                    {
                        sectionWidth = 0;
                        lastHeight--;
                    }
                    else if (nextMove == 1 && lastHeight < _height-_maxUpOffset-2) 
                    {
                        sectionWidth = 0;
                        lastHeight++;
                    }
                }

                sectionWidth++;

                _map.Set(x, lastHeight, tile);
            }
        }
    }
}

