using System.Collections;
using UnityEngine;
using Core.Map;

namespace Map.Generate
{
    public class PlatformMapModelGenerator : MapModelGeneratorBase
    {
        [SerializeField] private bool _ignorePlacedTiles = false;
        [Header("[from;to]")]
        [SerializeField, Min(1)] private Vector2Int _platformHeightRange = new Vector2Int(1, 3);
        [SerializeField, Min(1)] private Vector2Int _platformWidthRange = new Vector2Int(2, 3);
        [SerializeField, Min(1)] private Vector2Int _platformWidthOffset = new Vector2Int(1, 3);
        [SerializeField] private Vector2Int _platformHeightOffset = new Vector2Int(-1, 1);

        private int _targetHeight;
        private int _targetWidth;
        private int _curHeight;
        private int _curWidth;

        protected override void Generate()
        {
            CalcNewPlatformSize();
            _curHeight = _startPathHeight;

            for (int x = 0; x < _width; x++)
            {
                int yMin = _curHeight - _targetHeight;
                for (int y = _curHeight; y >= yMin && y >= 0; y--)
                {
                    if (!_ignorePlacedTiles && _map.Get(x, y) != MapModelElemType.None)
                    {
                        continue;
                    }
                    _map.Set(x, y, MapModelElemType.Platform);
                }
                
                _curWidth++;

                if (_curWidth >= _targetWidth)
                {
                    CalcNewPlatformSize();
                    
                    var offset = GetNewPlatformOffset();
                    x += offset.x;
                    _curHeight = Mathf.Clamp(_curHeight + offset.y, 0, _map.Height - 1);
                }
            }
        }
        private void CalcNewPlatformSize()
        {
            _curWidth = 0;

            _targetHeight = GetRandomFromVector(_platformHeightRange);
            _targetWidth = GetRandomFromVector(_platformWidthRange);
        }

        private Vector2Int GetNewPlatformOffset()
        {
            return new Vector2Int(
                  GetRandomFromVector(_platformWidthOffset)
                , GetRandomFromVector(_platformHeightOffset)
            );
        }

        private int GetRandomFromVector(Vector2Int range)
        {
            return Random.Range(range.x, range.y+1);
        }
    }
}

