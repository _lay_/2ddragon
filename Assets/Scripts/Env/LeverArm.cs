﻿using UnityEngine;

namespace Env
{
    public class LeverArm : Activatable
    {
        [SerializeField] Vector3 _enabledOffsetAngles = new Vector3(0, 0, 20);
        private Vector3 _rot;

        private void Start()
        {
            _rot = transform.rotation.eulerAngles;
        }

        protected override void ActivationAction()
        {
            transform.rotation = Quaternion.Euler(_rot + _enabledOffsetAngles);
        }

        protected override void DeactivationAction()
        {
            transform.rotation = Quaternion.Euler(_rot);
        }
    }
}
