﻿using System;
using UnityEngine;

namespace Env
{
    public class Activatable : MonoBehaviour
    {
        public bool Activated => _activated;

        public Action OnActivated;
        public Action OnDeactivate;

        [SerializeField] private bool _activated = false;
        [SerializeField] private bool _manualSound = false;
        [SerializeField] private AudioSource _activateSound;

        public void Activate()
        {
            if (!_activated)
            {
                _activated = true;
                if (!_manualSound)
                {
                    PlaySound();
                }
                ActivationAction();
                OnActivated?.Invoke();
            }
        }

        public void Deactivate()
        {
            if (_activated)
            {
                _activated = false;
                DeactivationAction();
                OnDeactivate?.Invoke();
            }
        }

#if UNITY_EDITOR
        private void Start()
        {
            _prevState = _activated;
        }

        private bool _prevState;
        private void Update()
        {
            if (_prevState != _activated)
            {
                _activated = _prevState;
                if (_prevState)
                {
                    Deactivate();
                }
                else
                {
                    Activate();
                }           
            }

            _prevState = _activated;
        }
#endif

        protected void PlaySound()
        {
            if (_activateSound != null && _activateSound.clip != null)
            {
                _activateSound.Play();
            }
        }

        protected virtual void ActivationAction()
        {
            
        }

        protected virtual void DeactivationAction()
        {

        }

    }
}
