using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class UIAddLabelButton : MonoBehaviour
    {
        public Image Icon => _Icon;
        public Text LabelText => _labelText;
        public Button Button => _button;
        public Text ButtonText => _buttonText;
        public RectTransform RectTransform => _rt;

        [SerializeField] private Image _Icon;
        [SerializeField] private Text _labelText;
        [Space]
        [SerializeField] private Button _button;
        [SerializeField] private Text _buttonText;
        
        private RectTransform _rt;


        private void Awake()
        {
            _rt = GetComponent<RectTransform>();
        }
    }
}
