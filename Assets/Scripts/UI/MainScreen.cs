﻿using UnityEngine;
using Core.UI;
using Game;

namespace UI
{
    public class MainScreen : UIScreen
    {
        [Space]
        [SerializeField] private UIMainMenu _main;
        [SerializeField] private UISettings _settngs;
        [SerializeField] private UIUpgradeByEnergy _upgrades;
        [SerializeField] private UIStatistics _statistics;
        [Space]
        [SerializeField] private ApplyCancelDialog _dialog;


        private void Awake()
        {
            GameDataProvider.ReloadAll();
        }

        public override void Init()
        {
            _dialog.HideDialog();
    
            _main.Init();
            _main.onNewGameClick += StartGame;
            _main.onSettingsClick += ShowSettings;
            _main.onUpgradesClick += ShowUpgrades;
            _main.onExitClick += ExitGameWithDialog;

            _settngs.Init();
            _settngs.onBackClick += ShowMain;
            _settngs.onResetClick += ResetProgres;

            _upgrades.Init();
            _upgrades.onBackClick += ShowMain;

            ShowMain();
        }

        public override void Deinit()
        {
            _dialog.HideDialog();

            _main.Deinit();
            _upgrades.Deinit();
            _settngs.Deinit();
        }

        private void StartGame()
        {
            NextScene();
        }

        private void ShowMain()
        {
            _main.gameObject.SetActive(true);
            _settngs.gameObject.SetActive(false);
            _upgrades.gameObject.SetActive(false);
        }

        private void ShowSettings()
        {
            _main.gameObject.SetActive(false);
            _settngs.gameObject.SetActive(true);
            _upgrades.gameObject.SetActive(false);
        }

        private void ShowUpgrades()
        {
            _main.gameObject.SetActive(false);
            _settngs.gameObject.SetActive(false);
            _upgrades.gameObject.SetActive(true);
        }

        private void ExitGameWithDialog()
        {
            if (_dialog != null)
            {
                _dialog.ShowDialog("Game exit", "Do you want to exit?",
                    () => ExitGame(),
                    () => _dialog.HideDialog()
                );
            }
            else
            {
                ExitGame();
            }
        }

        
        private void ResetProgres()
        {
            GameDataProvider.GameSave.Save(new GameSave());
        }
    }
}
