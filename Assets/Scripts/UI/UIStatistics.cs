using UnityEngine;
using Game;

namespace UI
{
    public class UIStatistics : MonoBehaviour
    {
        public UIGameValue _maxDistance;
        public UIGameValue _kills;
        public UIGameValue _deads;
        public UIGameValue _energy;

        private GameSaveProvider _gameSave => GameDataProvider.GameSave;
        
        private void Awake()
        {
            UpdateValues();
            _gameSave.onDataChanged += UpdateValues;
        }

        private void UpdateValues()
        {
            var save = _gameSave.Load();

            _maxDistance.Value.text = save.MaxDistance + " m";
            _kills.Value.text = save.EnemyKills.ToString();
            _deads.Value.text = save.Deads.ToString();
            _energy.Value.text = save.LifeEnergy.ToString();
        }

        private void OnDestroy()
        {
            _gameSave.onDataChanged -= UpdateValues;
        }
    }
}
