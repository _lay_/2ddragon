using UnityEngine;
using UnityEngine.UI;
using Game;
using System;

namespace UI
{
    public class UISettings : MonoBehaviour
    {
        public event Action onBackClick;
        public event Action onResetClick;

        public Toggle MusicToggle;
        public Slider MusicSlider;
        public Button ResetBtn;
        public Button BackBtn;

        private GameSettingsProvider _gameSettings => GameDataProvider.GameSettings;


        private void Awake()
        {
            _gameSettings.onDataChanged += LoadSettings;
        }

        public void Init()
        {
            LoadSettings();
            BackBtn.onClick.AddListener(() => onBackClick?.Invoke());
            ResetBtn.onClick.AddListener(() => onResetClick?.Invoke());
            MusicToggle.onValueChanged.AddListener(OnMusicEnabledChanged);
            MusicSlider.onValueChanged.AddListener(OnMusicVolumeChanged);
        }

        public void Deinit()
        {
            onBackClick = null;
            onResetClick = null;
            ResetBtn.onClick.RemoveAllListeners();
            BackBtn.onClick.RemoveAllListeners();
            MusicToggle.onValueChanged.RemoveAllListeners();
            MusicSlider.onValueChanged.RemoveAllListeners();
        }

        private void SaveSettings()
        {
            var data =_gameSettings.Load();
            data.SoundsEnabled = MusicToggle.isOn;
            data.SoundsVolume = MusicSlider.value;

           _gameSettings.Save(data);
        }

        private void LoadSettings()
        {
            var data =_gameSettings.Load();

            MusicToggle.SetIsOnWithoutNotify(data.SoundsEnabled);
            MusicSlider.value = data.SoundsVolume;
        }

        
        private void OnMusicEnabledChanged(bool value)
        {
            SaveSettings();
        }
        private void OnMusicVolumeChanged(float value)
        {
            SaveSettings();
        }

        private void OnDestroy()
        {
            _gameSettings.onDataChanged -= LoadSettings;
        }
    }
}
