using System;
using UnityEngine;
using UnityEngine.UI;
using Game;

namespace UI
{
    public class UIUpgradeByEnergy : MonoBehaviour
    {
        public event Action onBackClick;

        [SerializeField] private UIGameValue _lifeEnergy;
        [Space]
        [SerializeField] private UIGameValue _playerAttackGrade;
        [SerializeField] private UIAddLabelButton _playerAttackGradeCost;
        [Space]
        [SerializeField] private UIGameValue _playerHealthGrade;
        [SerializeField] private UIAddLabelButton _playerHealthGradeCost;
        [Space]
        [SerializeField] private Button _backButton;
        [Space]
        [SerializeField] private Color _upgradeDisabledColor = Color.red;
        private Color _upgradeEnabledColor;

        private const int _costGradeAdd = 10;

        private GameSaveProvider _gameSave => GameDataProvider.GameSave;


        private void Awake()
        {
            _gameSave.onDataChanged += UpdateData;

            _upgradeEnabledColor = _playerAttackGradeCost.ButtonText.color;
        }

        public void Init()
        {
            UpdateData();
            _backButton.onClick.AddListener(() => onBackClick?.Invoke());

            _playerAttackGradeCost.Button.onClick.AddListener(OnAttackGradeClick);
            _playerHealthGradeCost.Button.onClick.AddListener(OnHealthGradeClick);
        }

        public void Deinit()
        {
            onBackClick = null;
            _backButton.onClick.RemoveAllListeners();
            _playerAttackGradeCost.Button.onClick.RemoveAllListeners();
            _playerHealthGradeCost.Button.onClick.RemoveAllListeners();
        }

        private void UpdateData()
        {
            var save = _gameSave.Load();

            _lifeEnergy.Value.text = save.LifeEnergy.ToString();

            _playerAttackGrade.Value.text = save.PlayerAttackGrade.ToString();
            _playerHealthGrade.Value.text = save.PlayerHealthGrade.ToString();

            _playerAttackGradeCost.LabelText.text = "+" + GetNextCost(save.PlayerAttackGrade);
            _playerHealthGradeCost.LabelText.text = "+" + GetNextCost(save.PlayerHealthGrade);

            bool canUpgrade = GetNextCost(save.PlayerAttackGrade) <= save.LifeEnergy;
            _playerAttackGradeCost.Button.interactable = canUpgrade;
            _playerAttackGradeCost.ButtonText.color = canUpgrade ? _upgradeEnabledColor : _upgradeDisabledColor;
            canUpgrade = GetNextCost(save.PlayerHealthGrade) <= save.LifeEnergy;
            _playerHealthGradeCost.Button.interactable = canUpgrade;
            _playerHealthGradeCost.ButtonText.color = canUpgrade ? _upgradeEnabledColor : _upgradeDisabledColor;
        }

        private void OnAttackGradeClick()
        {
            var save = _gameSave.Load();

            int cost = GetNextCost(save.PlayerAttackGrade);
            if (cost <= save.LifeEnergy)
            {
                save.PlayerAttackGrade += 1;
                save.LifeEnergy -= cost;
                _gameSave.Save(save);
            }
        }

        private void OnHealthGradeClick()
        {
            var save = _gameSave.Load();

            int cost = GetNextCost(save.PlayerHealthGrade);
            if (cost <= save.LifeEnergy)
            {
                save.PlayerHealthGrade += 1;
                save.LifeEnergy -= cost;
                _gameSave.Save(save);
            }
        }

        private int GetNextCost(int curGrade)
        {
            return _costGradeAdd * (curGrade+1);
        }

        private void OnDestroy()
        {
            Deinit();
            _gameSave.onDataChanged -= UpdateData;
        }
    }
}
