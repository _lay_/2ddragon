﻿using UnityEngine;
using UnityEngine.UI;
using Core;
using Core.UI;
using Game;

namespace UI
{
    public class GameScreen : UIScreen
    {                
        [SerializeField] private Image _background;
        [SerializeField] private Button _pauseBtn;
        [SerializeField] private Text _distanceText;
        [SerializeField] private Text _lifeEnergyText;
        [SerializeField] private ApplyCancelDialog _dialog;
        [Space]
        [SerializeField] private UISettings _settings;
        [SerializeField] private UIUpgradeByEnergy _upgrades;

        private GameSaveProvider _gameSave => GameDataProvider.GameSave;

        public override void Init()
        {
            _dialog.HideDialog();
            _settings.Init();
            _upgrades.Init();
            ChangePause(false);
            _upgrades.gameObject.SetActive(false);
            _settings.gameObject.SetActive(false);
            _gameSave.onDataChanged += UpdateLifeEnergy;
            _pauseBtn.onClick.AddListener(ShowPauseDialog);
        }

        public override void Deinit()
        {
            _settings.Deinit();
            _upgrades.Deinit();
            _gameSave.onDataChanged -= UpdateLifeEnergy;
            _pauseBtn.onClick.RemoveAllListeners();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (_dialog.IsShowing)
                {
                    HidePauseDialog();
                }
                else
                {
                    ShowPauseDialog();
                }
            }
        }

        private void FixedUpdate()
        {
            _distanceText.text = GameInstance.Instance.GameMode.Distance + " m";
            UpdateLifeEnergy();
        }

        public void ShowDeadDialog()
        {
            if (_dialog.IsShowing) { return; }

            ChangePause(true);

            _dialog.ShowDialog("Game over", "You died",
                "Restart", RestartScene,
                "To menu", BackToMenu
            );
        }
        
        public void ShowFinishDialog()
        {
            if (_dialog.IsShowing) { return; }

            ChangePause(true);

            _dialog.ShowDialog("Game", "Game complete!",
                "Restart", RestartScene,
                "To menu", BackToMenu
            );
        }

        public void ShowPauseDialog()
        {
            if (_dialog.IsShowing) { return; }
            
            ChangePause(true);
            _upgrades.gameObject.SetActive(true);
            _settings.gameObject.SetActive(true);

            _dialog.ShowDialog("Pause", "Game paused",
                "Continue", () =>
                {
                    ChangePause(false);
                    _upgrades.gameObject.SetActive(false);
                    _settings.gameObject.SetActive(false);
                },
                "Exit", () => ShowExitDialog(false)
            );
        }

        public void HidePauseDialog()
        {
            if (!_dialog.IsShowing) { return; }

            ChangePause(false);
            _upgrades.gameObject.SetActive(false);
            _settings.gameObject.SetActive(false);
            _dialog.HideDialog();
        }

        public void ShowExitDialog(bool withAnim=true)
        {
            if (_dialog.IsShowing) { return; }
            
            ChangePause(true);
            _upgrades.gameObject.SetActive(false);
            _settings.gameObject.SetActive(false);

            _dialog.ShowDialog("Exit", "choise",
                "Exit Game", () => ExitGame(),
                "To menu", () => BackToMenu(),
                withAnim
            );
        }

        private void UpdateLifeEnergy()
        {
            _lifeEnergyText.text = GameInstance.Instance.GameMode.LifeEnergy.ToString();
        }

        private void ChangePause(bool paused)
        {
            SetGamePause(paused);
            _background.gameObject.SetActive(paused);
        }

        private void BackToMenu()
        {
            PrevScene();
            GameEvents.OnLevelEnd?.Invoke();
        }
    }
}
