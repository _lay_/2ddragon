using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class UIGameValue : MonoBehaviour
    {
        public Image Background => _background;
        public Image Icon => _icon;
        public Text Value => _value;
        public RectTransform RectTransform => _rt;

        [SerializeField] private Image _background;
        [SerializeField] private Image _icon;
        [SerializeField] private Text _value;
        
        private RectTransform _rt;


        private void Awake()
        {
            _rt = GetComponent<RectTransform>();
        }
    }
}
