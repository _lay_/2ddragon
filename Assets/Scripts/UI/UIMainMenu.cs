using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIMainMenu : MonoBehaviour
    {
        public event Action onNewGameClick;
        public event Action onSettingsClick;
        public event Action onUpgradesClick;
        public event Action onExitClick;

        public Button NewGameBtn;
        public Button SettingsBtn;
        public Button UpgradesBtn;
        public Button ExitBtn;

        public void Init()
        {
            NewGameBtn.onClick.AddListener(() => onNewGameClick?.Invoke());
            SettingsBtn.onClick.AddListener(() => onSettingsClick?.Invoke());
            UpgradesBtn.onClick.AddListener(() => onUpgradesClick?.Invoke());
            ExitBtn.onClick.AddListener(() => onExitClick?.Invoke());
        }

        public void Deinit()
        {
            onNewGameClick = null;
            onSettingsClick = null;
            onUpgradesClick = null;
            onExitClick = null;
            NewGameBtn.onClick.RemoveAllListeners();
            SettingsBtn.onClick.RemoveAllListeners();
            UpgradesBtn.onClick.RemoveAllListeners();
            ExitBtn.onClick.RemoveAllListeners();
        }
    }
}
