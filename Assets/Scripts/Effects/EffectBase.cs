using System;
using System.Collections;
using UnityEngine;

namespace Effects
{
    public class EffectBase : MonoBehaviour
    {
        protected void DelayedAction(float delay, Action action)
        {
            StartCoroutine(DelayedRoutine(delay, action));
        }

        private IEnumerator DelayedRoutine(float delay, Action action)
        {
            if (action != null)
            {
                yield return new WaitForSeconds(delay);
                action.Invoke();
            }   
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

    }
}
