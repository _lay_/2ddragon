using UnityEngine;
using Game;

namespace Effects
{
    public class TwoPointsEffect : EffectBase
    {
        [SerializeField] private ParticleSystem _start;
        [SerializeField] private ParticleSystem _end;
        [SerializeField] private Color _defaultColor = Color.gray;
        [SerializeField] private float _endEffectDelay = 1;

        private Vector2 _startPoint;
        private Transform _endTarget;

        public void StartEffect(Vector2 startPoint, Transform endTarget)
        {
            _startPoint = startPoint;
            _endTarget = endTarget;

            _start.startColor = _defaultColor;
            _end.startColor = _defaultColor;

            ActivateStart();
        }

        public void SetColor(Color value)
        {
            _defaultColor = value;
        }

        private void ActivateStart()
        {
            _start.gameObject.transform.position = _startPoint;
            _start.gameObject.SetActive(true);
            DelayedAction(_endEffectDelay, ActivateEnd);
        }

        private void ActivateEnd()
        {
            _end.gameObject.transform.position = (Vector2)_endTarget.position;
            _end.gameObject.SetActive(true);
            DelayedAction(_end.duration * 1.5f, () => Destroy(this.gameObject));
        }
    }
}
