using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Input
{
    public class MobileInput : MonoBehaviour
    {
        public float joystickVertical => _joystick.Vertical;
        public float joystickHorizontal => _joystick.Horizontal;
        
        [SerializeField] private Joystick _joystick;
        [SerializeField] private List<MubileButton> _buttons = new List<MubileButton>();        


        public void Start()
        {
            OnValidate();
        }
   
        public bool GetButton(MobileBtnType type)
        {
            MubileButton btn = FindButton(type);
            if (btn != null)
            {
                return btn.KeyPressed;
            }

            return false;
        }

        public bool GetButtonDown(MobileBtnType type)
        {
            MubileButton btn = FindButton(type);
            if (btn != null)
            {
                return btn.KeyDown;
            }

            return false;
        }

        private MubileButton FindButton(MobileBtnType type)
        {
            if (type == MobileBtnType.None) { return null; }

            for (int i = 0; i < _buttons.Count; i++)
            {
                MubileButton btn = _buttons[i];
                if (btn.Type == type)
                {
                    return btn;
                }
            }

            return null;
        }
        
        private void OnValidate()
        {
            if (_buttons != null && _buttons.Count > 0)
            {
                List<int> dublicates = new List<int>();

                for (int i = 0; i < _buttons.Count; i++)
                {
                    var type = _buttons[i].Type;

                    if (type != MobileBtnType.None)
                    {
                        for (int j = i+1; j < _buttons.Count; j++)
                        {
                            if (_buttons[j].Type == type && !dublicates.Contains(j))
                            {
                                dublicates.Add(j);
                            }
                        }
                    }
                }

                dublicates.ForEach(i =>
                {
                    Debug.LogWarning("[MobileInput][OnValidate] Find dublicate ButtonAction at index " + i);
                });
            }
        }
    }
}
