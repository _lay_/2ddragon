using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Core.Input
{
    [RequireComponent(typeof(Button))]
    public class MubileButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public bool KeyDown => _clicked;
        public bool KeyPressed => _pressed;

        public MobileBtnType Type => _type;

        [SerializeField] private MobileBtnType _type;

        private Button _button;
        private bool _pressed;
        private bool _clicked;


        private void Start()
        {
            _button = GetComponent<Button>();

            if (_type == MobileBtnType.None)
            {
                Debug.LogWarning("[MubileButton] Maybe need set btn type for " + gameObject.name);
            }
        }

        public void LateUpdate()
        {
            _clicked = false;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _clicked = true;
            _pressed = true;
        }
 
        public void OnPointerUp(PointerEventData eventData)
        {
            _pressed = false;
        }
    }
}
