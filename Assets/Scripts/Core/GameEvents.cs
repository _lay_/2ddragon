using System;

namespace Core
{
    public static class GameEvents
    {
        public static Action<bool> OnPauseChanged;
        public static Action OnLevelStarted;
        public static Action OnLevelEnd;
        public static Action OnPlayerDeath;
        public static Action OnEnemyKill;
        public static Action OnGameExit;
    }
}