using UnityEngine;

namespace Core
{

    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected static bool _dontDestroyOnLoad = true;
        protected static bool _createIfNotExsists = true;

        private static T _instance;

        private static object _lock = new object();

        public static T Instance
        {
            get
            {
                if (applicationIsQuitting)
                {
                    Debug.LogWarningFormat("[Singleton] Instance '{0}' already destroyed on application quit. Won't create again - returning null.", typeof(T));
                    return null;
                }

                lock (_lock)
                {
                    if (_instance == null)
                    {
                        T[] objects = FindObjectsOfType<T>();
                        if (objects.Length > 0)
                        {
                            _instance = objects[0];
                            if (objects.Length > 1)
                            {
                                Debug.LogError("[Singleton] Something went really wrong - there should never be more than 1 singleton! Reopening the scene might fix it.");
                                return _instance;
                            }
                        }
                        if (_instance == null)
                        {
                            if (_createIfNotExsists)
                            {
                                GameObject singleton = new GameObject();
                                _instance = singleton.AddComponent<T>();
                                singleton.name = string.Format("[Singleton] {0}", typeof(T));

                                if (_dontDestroyOnLoad)
                                {
                                    DontDestroyOnLoad(singleton);
                                    Debug.LogFormat("[Singleton] An instance of {0} is needed in the scene, so '{1}' was created with DontDestroyOnLoad.", typeof(T), singleton);
                                }
                            }
                            else
                            {
                                Debug.LogFormat("[Singleton] An instance of {0} is needed in the scene, but not created (_createIfNotExsists = false).", typeof(T));
                            }
                        }
                        else
                        {
                            if (_dontDestroyOnLoad)
                            {
                                // make DontDestroyOnLoad if not
                                // DontDestroyOnLoad(_instance.gameObject);

                                // Debug.LogFormat("[Singleton] Using instance already created: {0}", _instance.gameObject.name);
                            }
                        }
                    }
                    return _instance;
                }
            }
        }

        
        private static bool applicationIsQuitting = false;
        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed, 
        ///   it will create a buggy ghost object that will stay on the Editor scene
        ///   even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        internal virtual void OnDestroy()
        {
            
        }

        public void Shutdown()
        {
            applicationIsQuitting = true;
        }

        internal virtual void Awake() { }
    }
}
