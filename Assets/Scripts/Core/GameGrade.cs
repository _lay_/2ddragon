using System;
using UnityEngine;

namespace Core
{
    public class GameGrade
    {
        public event Action OnGradeChanged;

        public int CurrentGrade => _currentGrade;
        public float UpgradePercent => _upgradePercent;
        public float CurrentGradePercent => 1 + _upgradePercent * _currentGrade;

        [SerializeField] private int _currentGrade;
        [SerializeField] private float _upgradePercent;
        public GameGrade(float oneGradePercent=.1f)
        {
            _upgradePercent = oneGradePercent;
            Reset();
        }

        public void Upgrade()
        {
            _currentGrade++;
            OnGradeChanged?.Invoke();
        }
        public void SetUpgrade(int value)
        {
            _currentGrade = value;
            OnGradeChanged?.Invoke();
        }

        public void Reset()
        {
            SetUpgrade(0);
        }
    }
}