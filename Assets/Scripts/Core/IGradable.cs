
namespace Core
{
    public interface IGradable
    {
        public void ResetGrade();

        public void UpdateGrade(GameGrade grade);
    }
}