using Core.Units;
using UnityEngine;

namespace Core.UI
{
    public class UIHealthScreen : MonoBehaviour
    {
        [SerializeField] private GameObject _uiHealthPrefab;

        private ItemsPool<UIHealth> _pool;


        public void Init()
        {
            _pool = new ItemsPool<UIHealth>(this.gameObject, _uiHealthPrefab);
        }

        public void Deinit()
        {

        }

        private void OnValidate()
        {
            if (_uiHealthPrefab.GetComponent<UIHealth>() == null)
            {
                _uiHealthPrefab = null;
                Debug.LogError("[UIHealthScreen] Need atach UIHealth!");
            }
        }

        public void AtachNew(HPControl target, ref System.Action hideHook)
        {
            var ui = _pool.Take();
            ui.SetHPTarget(target);
            hideHook = () => _pool.ReleaseMatch(itm => itm.HP.GetHashCode() == target.GetHashCode());
            ui.Play();
        }

        private void OnDestroy()
        {
            _pool.ReleaseMatch(itm => true);
        }
    }
}
