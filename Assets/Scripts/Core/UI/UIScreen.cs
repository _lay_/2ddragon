﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.UI
{
    public class UIScreen : MonoBehaviour
    {
        protected bool _isPaused => Time.timeScale == 0f;
        [SerializeField] private AudioSource _backgroundMusic;
        [SerializeField] private bool _initOnStart = false;


        private void Start()
        {
            if (_initOnStart)
            {
                Init();
            }

        }

        public virtual void Init()
        {

        }

        public virtual void Deinit()
        {

        }

        public void Hide()
        {
            gameObject.SetActive(true);
        }

        protected void SetGamePause(bool paused)
        {
            Time.timeScale = paused ? 0f : 1f;

            GameEvents.OnPauseChanged?.Invoke(paused);
        }

        protected void RestartScene()
        {
            ChangeScene(SceneManager.GetActiveScene().buildIndex);
        }

        protected void NextScene()
        {
            ChangeScene(SceneManager.GetActiveScene().buildIndex+1);
        }

        protected void PrevScene()
        {
            ChangeScene(SceneManager.GetActiveScene().buildIndex-1);
        }

        protected void ChangeScene(int newSceneId)
        {
            SceneManager.LoadSceneAsync(newSceneId, LoadSceneMode.Single);
        }

        protected void ExitGame()
        {
    #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
    #else
            Application.Quit();
    #endif
            GameEvents.OnGameExit?.Invoke();
        }

        private void OnDestroy()
        {
            Deinit();
        }
    }
}
