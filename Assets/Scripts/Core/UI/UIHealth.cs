using UnityEngine;
using UnityEngine.UI;
using Core.Units;
using System;

namespace Core.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class UIHealth : MonoBehaviour, IPlayable
    {
        public HPControl HP => _hp;
        public bool IsPlaying => _isPlaying;

        public event Action OnPlay;
        public event Action OnStop;

        [SerializeField] private Slider _slider;
        [SerializeField] private HPControl _hp;
        [SerializeField] private float _yOffset = 1f;

        private Camera _cam;
        private RectTransform _rt;
        private bool _isPlaying = false;


        private void Awake()
        {
            _rt = GetComponent<RectTransform>();
            gameObject?.SetActive(false);

#if UNITY_EDITOR
            if (_rt.anchorMin.x != _rt.anchorMax.x || _rt.anchorMin.y != _rt.anchorMax.y)
            {
                Debug.LogError("[UIHealth] Wrong anchors!");
            }
#endif
        }

        public void SetHPTarget(HPControl hp)
        {
            if (_hp != null)
            {
                UnsignCallbacks();
            }
            _hp = hp;
        }
        
        public void Play()
        {
            if (_hp == null || _isPlaying) { return; }

            _cam = Camera.main;
            SignCallbacks();
            UpdatePoints();
            gameObject?.SetActive(true);
            _isPlaying = true;
            OnPlay?.Invoke();
        }

        public void Stop()
        {
            if (!_isPlaying) { return; }

            if (_hp != null)
            {
                UnsignCallbacks();
            }
            _hp = null;
            gameObject?.SetActive(false);
            _isPlaying = false;
            OnStop?.Invoke();
        }
        
        private void Update()
        {
            if (!_isPlaying) { return; }

            var point = _hp.gameObject.transform.position + Vector3.up * _yOffset;
            var viewPort = _cam.WorldToViewportPoint(point);

            if (viewPort.x < .1f || viewPort.x > 1.1f)
            {
                _slider.gameObject.SetActive(false);
                return;
            }

            _slider.gameObject.SetActive(true);
            _rt.anchorMin = viewPort;
            _rt.anchorMax = viewPort;
        }

        private void SignCallbacks()
        {
            _hp.OnPointsChanged += UpdatePoints;
            _hp.OnMaxPointsChanged += UpdatePoints;
        }

        private void UnsignCallbacks()
        {
            _hp.OnPointsChanged -= UpdatePoints;
            _hp.OnMaxPointsChanged -= UpdatePoints;
        }

        private void UpdatePoints()
        {
            _slider.value = (float)_hp.CurPoints / _hp.MaxPoints;
        }

        private void OnDestroy()
        {
            _isPlaying = false;
        }
    }
}
