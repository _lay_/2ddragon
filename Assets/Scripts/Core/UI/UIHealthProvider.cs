using System;
using Core.Units;
using Game;

namespace Core.UI
{
    public class UIHealthProvider
    {       
        private Action _hideHook;


        public void Attach(HPControl _hp)
        {
            GameInstance.Instance.HealthScreen.AtachNew(_hp, ref _hideHook);
        }

        public void Deattach()
        {
            _hideHook?.Invoke();
            _hideHook = null;
        }
    }
}
