﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class ApplyCancelDialog : MonoBehaviour
    {
        public bool IsShowing => _isShowing;

        [SerializeField] private Animator _animator;
        [SerializeField] private Text _title;
        [SerializeField] private Text _message;
        [SerializeField] private Button _applyBtn;
        [SerializeField] private Button _cancelBtn;
        [SerializeField] private Text _applyText;
        [SerializeField] private Text _cancelText;
        
        private Action _onApply;
        private Action _onCancel;

        private bool _isShowing;

        private void Start()
        {
            _applyBtn.onClick.AddListener(OnApply);
            _cancelBtn.onClick.AddListener(OnCancel);
        }

        public void ShowDialog(string title, string message, Action onApply=null, Action onCancel=null)
        {
            ShowDialog(title, message, "", onApply, "", onCancel);
        }

        public void ShowDialog(string title, string message, string applyText="",  Action onApply=null, string cancelText="", Action onCancel=null, bool withAnim=true)
        {
            Reset();

            _isShowing = true;

            _title.text = title;
            _message.text = message;
            if (!string.IsNullOrEmpty(applyText))
            {
                _applyText.text = applyText;
            }
            if (!string.IsNullOrEmpty(cancelText))
            {
                _cancelText.text = cancelText;
            }

            _onApply = onApply;
            _onCancel = onCancel;

            if (_animator != null)
            {
                _animator.enabled = withAnim;
            }
            
            gameObject.SetActive(_isShowing);
        }

        public void HideDialog()
        {
            Reset();
        }

        private void Reset()
        {
            _isShowing = false;
            gameObject.SetActive(_isShowing);
            
            _title.text = 
            _message.text = "Some text";
            _applyText.text = "Apply";
            _cancelText.text = "Cancel";
        }

        private void OnApply()
        {
            Reset();
            _onApply?.Invoke();
        }

        private void OnCancel()
        {
            Reset();
            _onCancel?.Invoke();
        }

        private void OnDestroy()
        {
            _onApply = null;
            _onCancel = null;
            _applyBtn.onClick.RemoveAllListeners();
            _cancelBtn.onClick.RemoveAllListeners();
        }
    }
}
