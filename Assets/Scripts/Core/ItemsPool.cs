using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class ItemsPool<T> where T : IPlayable
    {
        public int CountUsed => _usedItems.Count;
        public int CountUnused => _unusedItems.Count;

        private GameObject _containerGO;
        private GameObject[] _itemPrefabs;

        private List<T> _unusedItems = new List<T>();
        private List<T> _usedItems = new List<T>();


        public ItemsPool(GameObject containerGO, GameObject[] itemPrefabs)
        {
            _containerGO = containerGO;
            _itemPrefabs = itemPrefabs;
        }
        public ItemsPool(GameObject containerGO, GameObject itemPrefab)
        {
            _containerGO = containerGO;
            _itemPrefabs = new GameObject[] { itemPrefab };
        }

        public T Take(bool activate=false)
        {
            T item;
            if (_unusedItems.Count > 0)
            {
                item = _unusedItems[0];
                _unusedItems.RemoveAt(0);
            }
            else
            {
                var go = GameObject.Instantiate(GetRandomItemPrefab(), _containerGO.transform);
                item = go.GetComponent<T>();
            }

            _usedItems.Add(item);
            if (activate)
            {
                item.Play();
            }

            return item;
        }

        public void ReleaseMatch(System.Predicate<T> predicate, bool tryStop=true)
        {
            for (int i = 0; i < CountUsed; i++)
            {
                if (predicate.Invoke(_usedItems[i]))
                {
                    ReleaseAt(i, tryStop);
                    i--;
                }
            }
        }

        public void Release(int index, bool tryStop=true)
        {
            T item = _usedItems[index];
            if (tryStop && item.IsPlaying)
            {
                item?.Stop();
            }
            _unusedItems.Add(item);
            _usedItems.RemoveAt(index);
        }

        public void ReleaseAt(int index, bool tryStop=true)
        {
            T item = _usedItems[index];
            if (tryStop && item.IsPlaying)
            {
                item?.Stop();
            }
            _unusedItems.Add(item);
            _usedItems.RemoveAt(index);
        }

        private GameObject GetRandomItemPrefab()
        {
            if (_itemPrefabs == null || _itemPrefabs.Length == 0)
            {
                Debug.LogError("[EnvPool] prefabs not inited!");
            }
            
            int i = Random.Range(0, _itemPrefabs.Length);
            return _itemPrefabs[i];
        }
    }
}

