using System.IO;
using UnityEngine;

namespace Core
{
    public static class PersistanseProvider
    {
        private const string _fileExtention = ".json";

        public static void Save<T>(string saveName, T obj)
        {
            var json = JsonUtility.ToJson(obj);

            File.WriteAllText(GetFileDataPath(saveName), json);
        }

        public static T Load<T>(string saveName)
        {
            var json = File.ReadAllText(GetFileDataPath(saveName));
            return JsonUtility.FromJson<T>(json);
        }

        public static bool LoadOverride<T>(string saveName, T obj) where T : MonoBehaviour
        {
            if (!Exists(saveName)) { return false; }

            var json = File.ReadAllText(GetFileDataPath(saveName));

            try
            {
                JsonUtility.FromJsonOverwrite(json, obj);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool Exists(string saveName)
        {
            return File.Exists(GetFileDataPath(saveName));
        }

        private static string GetFileDataPath(string fileName)
        {
            return Path.Combine(Application.persistentDataPath, fileName + _fileExtention);
        }
    }
        
}
