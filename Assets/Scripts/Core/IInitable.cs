
namespace Core
{
    public interface IInitable
    {
        void Init();
        void Deinit(); 
    }
}
