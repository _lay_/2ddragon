using System;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Core.Map
{
    public enum MapLayers
    {
        Background,
        BackgroundEnv,
        Main,
        ForegroundEnv,
        Foreground,
    }

    [System.Serializable]
    public class LayeredTileMap
    {
        public Tilemap Background;
        public Tilemap BackgroundEnv;
        public Tilemap Main;
        public Tilemap ForegroundEnv;
        public Tilemap Foreground;
        public GameObject Envs;

        private Array _mapTypes => Enum.GetValues(typeof(MapLayers));


        public void ResetMaps()
        {
            Array maps = _mapTypes;

            for (int i = 0; i < maps.Length; i++)
            {
                ResetMap((MapLayers)maps.GetValue(i));
            }           
        }
        public void ResetMap(MapLayers layer)
        {
            GetMap(layer).ClearAllTiles();
        }

        public void ClearMaps(int startX, int endX, int height)
        {
            Array maps = _mapTypes;
            for (int i = 0; i < maps.Length; i++)
            {
                ClearMap((MapLayers)maps.GetValue(i), startX, endX, height);
            }   
        }
        public void ClearMap(MapLayers layer, int startX, int endX, int height)
        {
            if (startX == endX) { return; }

            var map = GetMap(layer);
            for (int x = startX; x < endX; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    map.SetTile(new Vector3Int(x, y, 0), null);
                }
            }
        }
        
        public Vector3 GetCenterWorldPoint(int x, int y)
        {
            return Main.GetCellCenterWorld(GetPoint(x, y));
        }
        public Vector3 GetDownWorldPoint(int x, int y)
        {
            var p = Main.GetCellCenterWorld(GetPoint(x, y));
            return new Vector3(p.x, p.y - Main.cellSize.y * .5f);
        }
        
        public Vector3Int GetPoint(int x, int y)
        {
            return new Vector3Int(x, y, 0);
        }

        public void AddTileTo(MapLayers layer, int x, int y, TileBase tile)
        {
            SetTile(GetMap(layer), GetPoint(x, y), tile);
        }
        private void AddTileTo(MapLayers layer, Vector3Int point, TileBase tile)
        {
            SetTile(GetMap(layer), point, tile);
        }

        private Tilemap GetMap(MapLayers layer)
        {
            switch (layer)
            {
                case MapLayers.Main:
                    return Main;
                case MapLayers.Background:
                    return Background;
                case MapLayers.Foreground:
                    return Foreground;
                case MapLayers.BackgroundEnv:
                    return BackgroundEnv;
                case MapLayers.ForegroundEnv:
                    return ForegroundEnv;
                default:
                    return Main;
            }
        }

        private void SetTile(Tilemap map, Vector3Int point, TileBase tile)
        {
            map.SetTile(point, tile);
        }
    }
}

