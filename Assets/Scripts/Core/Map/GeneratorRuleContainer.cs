using System;
using UnityEngine;
using Core.Map.Rhythm;

namespace Core.Map
{
    [System.Serializable]
    public class GeneratorRuleContainer
    {
        public float MinGroupDuration = 30f;
        public float MaxGroupDuration = 40f;
        public BeatType BeatType = BeatType.REGULAR;
        public float Density = .4f;
        public ActTypesFreq[] TypeFreqs = new ActTypesFreq[]
        {
            new ActTypesFreq(ActionType.Move, .3f),
            new ActTypesFreq(ActionType.Jump, .4f),
            new ActTypesFreq(ActionType.TwoWay, .1f),
            new ActTypesFreq(ActionType.Run, .1f),
            new ActTypesFreq(ActionType.Wait, .1f),
            new ActTypesFreq(ActionType.Fight, .4f),
        };
        public ActTypesOrderConstraint[] _typeConstrains = new ActTypesOrderConstraint[]
        {
            new ActTypesOrderConstraint(ActionType.Wait, ActionType.Run, ActTypesOrderConstraint.ConstraintWay.Both),
            new ActTypesOrderConstraint(ActionType.Wait, ActionType.TwoWay, ActTypesOrderConstraint.ConstraintWay.LeftToRight),
            new ActTypesOrderConstraint(ActionType.Wait, ActionType.Wait, ActTypesOrderConstraint.ConstraintWay.LeftToRight),
            new ActTypesOrderConstraint(ActionType.Run, ActionType.Jump, ActTypesOrderConstraint.ConstraintWay.LeftToRight),
        };

        public MapModelGeneratorBase[] Generators;
        public MapWriterBase[] Writers;

        private RhythmGroupGenerator _generator;
        private RhythmGroup _lastRhythym;


        public void GenerateAndWriteAll(MapModel map, int startPathHeight, int startWriteX)
        {
            GenerateRhythm();

            GenerateAll(_lastRhythym, map, startPathHeight);
            Write(map, startWriteX);
        }

        public void GenerateAll(RhythmGroup rhythm, MapModel map, int startPathHeight)
        {
            Array.ForEach(Generators, g => { g.Generate(rhythm, map, startPathHeight); });
        }

        public void Write(MapModel map, int startWriteX)
        {
            Array.ForEach(Writers, w => { w.SetStartX(startWriteX); w.Write(map); });
        }

        public void SetTileMapAll(LayeredTileMap map)
        {
            Array.ForEach(Writers, w => w.SetTileMap(map));
        }

        private void GenerateRhythm()
        {
            _generator = new RhythmGroupGenerator(MinGroupDuration, MaxGroupDuration, Density, BeatType, TypeFreqs, _typeConstrains);
            _lastRhythym = _generator.GenerateRhythmGroup();
        }
    }
}
