using System.Collections.Generic;

namespace Core.Map.Rhythm
{
    public class RhythmGroup
    {
        public Action[] Actions => _actions.ToArray();
        public float Duration => _duration;

        private List<Action> _actions;
        private float _duration;


        public RhythmGroup(float duration)
        {
            _actions = new List<Action>();
            _duration = duration;
        }

        public bool AddAction(ActionType type, float startTime, float duration)
        {
            if (startTime > _duration) { return false; }

            if (startTime + duration > _duration)
            {
                duration = _duration - startTime;
            }

            _actions.Add(new Action(type, startTime, duration));
            return true;
        }
    }
}