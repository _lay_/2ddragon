
namespace Core.Map.Rhythm
{
    public enum BeatType
    {
        REGULAR,
        RANDOM,
        SWING,
    }
}