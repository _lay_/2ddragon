using System;

namespace Core.Map.Rhythm
{
    public class BeatPattern
    {
        public int Length => _beatTimes.Length;
        public float[] BeatTimes => _beatTimes;
        public BeatType Type => _type;

        private float[] _beatTimes;
        private BeatType _type;
        
        public BeatPattern()
        {
            _type = BeatType.REGULAR;
            _beatTimes = new float[] { 0f, 1f };
        }

        public void Generate(BeatType type, float duration, float density)
        {
            int count = (int)Math.Floor(duration * density);
            if (count < 2)
            {
                throw new ArgumentOutOfRangeException($"BeatPattern is small (duration = {duration} and density = {density})!");
            }

            _beatTimes = new float[count];

            float shortBeat = duration / (2 * count -1f);
            float longBeat = 3 * shortBeat;
            var rand = new Random();

            for (int i = 0; i < count; i++)
            {
                float time = 0;
                if (type == BeatType.REGULAR)
                {
                    time = i * (duration * 1f / count);
                }
                else if (type == BeatType.RANDOM)
                {
                    time = (float)rand.NextDouble() * duration;
                }
                else if (type == BeatType.SWING)
                {
                    time = i % 2 == 0
                        ? (i / 2) * (longBeat + shortBeat)
                        : ((i - 1) / 2) * (longBeat + shortBeat) + longBeat;
                }

                _beatTimes[i] = time;
            }

            if (type == BeatType.RANDOM)
            {
                Array.Sort(_beatTimes);
            }
        }
    }
}