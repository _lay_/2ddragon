
namespace Core.Map.Rhythm
{
    public enum ActionType
    {
        Move,
        Jump,
        TwoWay,
        Run,
        Wait,
        Fight,
    }
}