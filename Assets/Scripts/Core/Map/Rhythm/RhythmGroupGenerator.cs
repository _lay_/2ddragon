using System;

namespace Core.Map.Rhythm
{
    [System.Serializable]
    public struct ActTypesFreq
    {
        public ActionType Type;
        public float Freq;

        public ActTypesFreq(ActionType type, float freq)
        {
            Type = type;
            Freq = freq;
        }
    }

    [System.Serializable]
    public struct ActTypesOrderConstraint
    {
        public enum ConstraintWay { LeftToRight, RightToLeft, Both}

        public ActionType LeftType;
        public ActionType RightType;
        public ConstraintWay Way;

        public ActTypesOrderConstraint(ActionType LeftType, ActionType rightType, ConstraintWay way)
        {
            this.LeftType = LeftType;
            RightType = rightType;
            Way = way;
        }

        public bool Check(ActionType one, ActionType two)
        {
            bool leftToRight = LeftType == one && RightType == two;
            bool rightToLeft = LeftType == two && RightType == one;

            switch (Way)
            {
                case ConstraintWay.LeftToRight:
                    return leftToRight;
                case ConstraintWay.RightToLeft:
                    return rightToLeft;
                case ConstraintWay.Both:
                    return leftToRight && rightToLeft;
                default:
                    return false;
            }
        }
    }

    public class RhythmGroupGenerator
    {
        private float _minGroupDuration;
        private float _maxGroupDuration;
        private float _density;
        // private float[] _beatFrequencies;
        private BeatType _beatType;
        private BeatPattern _pattern;
        private ActTypesFreq[] _actionsFreq;
        private ActTypesOrderConstraint[] _actionsConstrains;

        private Random _rand;


        public RhythmGroupGenerator(float minGroupDuration, float maxGroupDuration, float density, BeatType beatType, ActTypesFreq[] actionsFreq, ActTypesOrderConstraint[] actionsConstrains=null)
        {
            _minGroupDuration = minGroupDuration;
            _maxGroupDuration = maxGroupDuration;
            _density = density;
            _beatType = beatType;
            _pattern = new BeatPattern();
            _rand = new Random();
            _actionsFreq = actionsFreq;
            _actionsConstrains = actionsConstrains;
        }

        public RhythmGroup GenerateRhythmGroup()
        {
            var _rand = new Random();

            float groupDuration = _minGroupDuration == _maxGroupDuration
                ? _maxGroupDuration
                : _rand.Next(0, 11) * .1f * (_maxGroupDuration - _minGroupDuration) + _minGroupDuration;

            RhythmGroup group = new RhythmGroup(groupDuration);
            _pattern.Generate(_beatType, groupDuration, _density);

            ActionType prevType = ActionType.Move;
            float last = _pattern.BeatTimes[0];
            for (int i = 1; i < _pattern.Length; i++)
            {
                var type = GetFilteredRandType(prevType);
                
                group.AddAction(type, last, _pattern.BeatTimes[i] - last);

                last = _pattern.BeatTimes[i];
                prevType = type;
            }
            group.AddAction(ActionType.Move, last, groupDuration - last);

            return group;
        }

        private ActionType GetFilteredRandType(ActionType prevType)
        {
            var type = GetRandType();

            while (!CanSetType(prevType, type))
            {
                type = GetRandType();
            }

            return type;
        }

        private ActionType GetRandType()
        {
            float r = _rand.Next(0, (int)(GetFreqsSum() * 1000)) * .001f;
            float cumulative = 0;
            var type = ActionType.Move;
            for (int i = 0; i < _actionsFreq.Length; i++)
            {
                cumulative += _actionsFreq[i].Freq;
                if (cumulative > r)
                {
                    type = _actionsFreq[i].Type;
                    break;
                }
            }
            
            return type;
        }

        private bool CanSetType(ActionType prev, ActionType next)
        {
            bool res = true;

            foreach (var constraint in _actionsConstrains)
            {
                if (constraint.Check(prev, next))
                {
                    res = false;
                    break;
                }
            }

            return res;
        }

        private float GetFreqsSum()
        {
            float res = 0;
            for (int i = 0; i < _actionsFreq.Length; i++)
            {
                res += _actionsFreq[i].Freq;
            }

            return res;
        }
    }
}