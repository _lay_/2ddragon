
namespace Core.Map.Rhythm
{
    public class Action
    {
        public ActionType Type;
        public float StartTime;
        public float Duration;

        public Action(ActionType type, float startTime, float duration)
        {
            Type = type;
            StartTime = startTime;
            Duration = duration;
        }

        public override string ToString()
        {
            return $"Action [{Type}: {StartTime}, {Duration}]";
        }
    }
}