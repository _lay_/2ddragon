using UnityEngine;

namespace Core.Map
{
    public abstract class PooledMapWriterBase<T> : MapWriterBase, IPooledEnvWriter where T : IPlayable
    {
        [SerializeField] protected GameObject[] _envItemPrefabs;

        private ItemsPool<T> _envsPool;


        protected override void PreWrite()
        {
            InitPool();
        }

        public void ClearEnvItems(int rigthBoundX)
        {
            float clearX = _tileMap.GetCenterWorldPoint(rigthBoundX, 0).x;
            
            _envsPool?.ReleaseMatch(itm => itm.gameObject.transform.position.x < clearX);
        }

        public void ClearAllEnvItems()
        {
            _envsPool?.ReleaseMatch(itm => true);
        }

        private void InitPool()
        {
            if (_envsPool != null) { return; }

            var go = new GameObject(typeof(T).Name + " Container");
            go.transform.SetParent(_tileMap.Envs.transform);
                
            _envsPool = new ItemsPool<T>(go, _envItemPrefabs);
        }

        protected T GetEnvItem()
        {
            return _envsPool.Take();
        }
    }
}

