using Core.Map.Rhythm;
using UnityEngine;

namespace Core.Map
{
    public abstract class MapModelGeneratorBase : MonoBehaviour, IMapModelGenerator
    {
        protected int _height => _map.Height;
        protected int _width => _map.Width;
        
        protected MapModel _map;
        protected RhythmGroup _rhythm;
        protected int _startPathHeight;

        private float _rhythmNormalizer;

        public void Generate(RhythmGroup rhythm, MapModel map, int startPathHeight)
        {
            _map = map;
            _rhythm = rhythm;
            _startPathHeight = startPathHeight;

            _rhythmNormalizer = _width / _rhythm.Duration;

            PreGenerate();
            Generate();
            PostGenerate();

            _map = null;
            _rhythm = null;
            _rhythmNormalizer = 0;
        }

        protected abstract void Generate();
        protected virtual void PreGenerate() { }
        protected virtual void PostGenerate() { }

        protected int GetActionStartMapPoint(int actionIndex)
        {
            if (actionIndex <= 0)
            {
                return 0;
            }

            return GetRhythmMapPoint(_rhythm.Actions[actionIndex].StartTime);
        }
        protected int GetActionEndMapPoint(int actionIndex)
        {
            if (actionIndex+1 >= _rhythm.Actions.Length)
            {
                return _width;
            }

            return GetRhythmMapPoint(_rhythm.Actions[actionIndex+1].StartTime);
        }
        protected int GetRhythmMapPoint(float time)
        {
            if (time < 0)
            {
                return 0;
            }
            if (time >= _rhythm.Duration)
            {
                return _width;
            }

            return Mathf.FloorToInt(time * _rhythmNormalizer);
        }
    }
}

