
namespace Core.Map
{
    public interface IMapWriter
    {
        public void Write(MapModel model);
        public void SetTileMap(LayeredTileMap map);
        public void SetStartX(int value);
    }
}

