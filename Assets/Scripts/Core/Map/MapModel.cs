
namespace Core.Map
{
    public enum MapModelElemType
    {
        None = 0,
        Ground = 1,
        Platform,
        Enemy,
        Bridge,
        Climb,
        MovableHit,
        Movable,
    }

    public class MapModel
    {
        public int[,] Map => _map;
        public int Width => GetLength(0);
        public int Height => GetLength(1);

        private int[,] _map;

        public MapModel(int w, int h)
        {
            _map = new int[w, h];
        }

        public int GetLength(int dimension)
        {
            return _map.GetLength(dimension);
        }

        public bool Set(int x, int y, MapModelElemType value)
        {
            return Set(x, y, (int)value);
        }

        public bool Set(int x, int y, int value)
        {
            if (_map.GetLength(0) <= x || _map.GetLength(1) <= y)
            {
                return false;
            }

            _map[x, y] = value;
            return true;
        }

        public MapModelElemType Get(int x, int y)
        {
            return (MapModelElemType)_map[x, y];
        }
    }
}

