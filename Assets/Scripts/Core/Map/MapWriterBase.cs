using System;
using UnityEngine;

namespace Core.Map
{
    public abstract class MapWriterBase : MonoBehaviour, IMapWriter
    {
        protected MapModel _model;
        protected LayeredTileMap _tileMap;
        protected int _startX = 0;

        public void Write(MapModel model)
        {
            _model = model;

            PreWrite();
            Write();
            PostWrite();

            _model = null;
        }

        public void SetTileMap(LayeredTileMap map)
        {
            _tileMap = map;
        }
        public void SetStartX(int value)
        {
            _startX = value;
        }

        protected virtual void PreWrite() { }
        protected abstract void Write();
        protected virtual void PostWrite() { }

        protected void ForEachElement(Action<int, int> action)
        {
            if (action == null) { return; }

            int w = _model.Width;
            int h = _model.Height;

            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    action?.Invoke(x, y);
                }
            }
        }
    }
}

