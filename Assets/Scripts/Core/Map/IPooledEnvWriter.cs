
namespace Core.Map
{
    public interface IPooledEnvWriter
    {
        public void ClearEnvItems(int rigthBoundX);
        public void ClearAllEnvItems();
    }
}

