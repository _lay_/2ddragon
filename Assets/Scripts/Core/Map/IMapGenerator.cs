using Core.Map.Rhythm;

namespace Core.Map
{
    public interface IMapModelGenerator
    {
        void Generate(RhythmGroup rhythm, MapModel map, int startPathHeight);
    }
}
