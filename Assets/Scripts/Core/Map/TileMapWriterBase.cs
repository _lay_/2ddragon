using UnityEngine;
using UnityEngine.Tilemaps;

namespace Core.Map
{
    public abstract class TileMapWriterBase : MapWriterBase
    {
        [SerializeField] protected MapLayers _targetMapLayer = MapLayers.Main;
        [SerializeField] protected MapModelElemType _targetTileType = MapModelElemType.None;
        [SerializeField] protected TileBase _regularTile;
        
        
        protected void WriteNotNoneAt(MapLayers layer,  TileBase tile)
        {
            ForEachElement((x, y) =>
            {
                var placed = _model.Get(x, y);
                bool canPlace = _targetTileType ==  MapModelElemType.None || _targetTileType == placed;

                if (placed != MapModelElemType.None && canPlace)
                {
                    _tileMap.AddTileTo(layer, x + _startX, y, tile);
                }
            });
        }

        // protected override abstract void Write();
    }
}

