﻿using System;
using System.Collections;
using UnityEngine;

namespace Core.Units
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public abstract class UnitBase : MonoBehaviour
    {
        public event Action OnDied;

        [SerializeField] protected Transform _modelTS;
        [SerializeField] protected Animator _animator;
        [SerializeField] protected AttackControl _attackControl;
        [SerializeField] protected HPControl _hpControl;
        [SerializeField] protected AudioSource _audioSource;
        
        protected Rigidbody2D _rb;
        protected Collider2D _collider;

        protected FacingFlipControl _facingControl;

        private void OnEnable()
        {
            if (_facingControl == null)
            {
                _facingControl = new FacingFlipControl(_modelTS);
            }
        }

        private void Awake()
        {
            _rb = GetComponent<Rigidbody2D>();
            _collider = GetComponent<Collider2D>();

            _hpControl.OnPointsEnds += OnUnitDied;
        }

        private void Start()
        {
            InitGrades();
            Init();
        }

        protected virtual void Init()
        {
            
        }
        protected virtual void Deinit()
        {
            
        }

        protected virtual void InitGrades()
        {
            
        }
        protected virtual void DeinitGrades()
        {
            
        }

        protected void PlaySound(AudioClip sound)
        {
            _audioSource.clip = sound;
            if (!_audioSource.isPlaying)
            {
                _audioSource.Play();
            }
        }

        protected void DelayedAction(float delay, Action action)
        {
            StartCoroutine(DelayedRoutine(delay, action));
        }

        private IEnumerator DelayedRoutine(float delay, Action action)
        {
            if (action != null)
            {
                yield return new WaitForSeconds(delay);
                action.Invoke();
            }   
        }

        private void OnUnitDied()
        {
            OnDied?.Invoke();
        }

        private void OnDestroy()
        {
            _hpControl.OnPointsEnds -= OnUnitDied;
            StopAllCoroutines();
            Deinit();
            DeinitGrades();
        }
    }
}
