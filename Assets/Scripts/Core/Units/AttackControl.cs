﻿using System;
using UnityEngine;

namespace Core.Units
{
    public class AttackControl : HPDamage
    {
        [SerializeField] private float _attackRadius = 1;


        public bool TryPointAttack(Vector2 point)
        {
            RaycastHit2D[] hits = Physics2D.CircleCastAll(point, _attackRadius, Vector2.zero, 0, _layerMask);

            if (hits != null && hits.Length > 0)
            {
                bool attacked = false;
                Array.ForEach(hits, itm => 
                {
                    HPControl hp = itm.transform.gameObject.GetComponent<HPControl>();

                    attacked = TryAttack(hp) || attacked;
                });

                return attacked;
            }

            return false;
        }
    }
}
