﻿using System;
using UnityEngine;

namespace Core.Units
{
    public class HPControl : MonoBehaviour, IGradable
    {
        public int MaxPoints => _maxPoints;
        public int MinPoints => _minPoints;
        public int CurPoints => _curPoints;

        public bool IsAlive => _curPoints > _minPoints;

        public event Action  OnPointsChanged;
        public event Action  OnMaxPointsChanged;
        public event Action  OnPointsEnds;

        [SerializeField] private int _minPoints = 0;
        [SerializeField] private int _maxPoints = 100;
        [SerializeField] private int _curPoints = 100;
        // [SerializeField] private int _maxLives;
        // [SerializeField] private int _curLives;
        
        private int _startMaxPoints = -1;

        private void Awake()
        {
            _startMaxPoints = _maxPoints;
        }

        public void AddDamage(int damage)
        {
            AddPoints(-damage);
        }

        public void Heal(int heal)
        {
            AddPoints(heal);
        }

        public void ResetHP()
        {
            if (_curPoints != _maxPoints)
            {
                int sub = _maxPoints - _curPoints;
                _curPoints = _maxPoints;
                OnPointsChanged?.Invoke();
            }   
        }

        public void SetMaxPoints(int value)
        {
            _maxPoints = value;
            OnMaxPointsChanged?.Invoke();
        }

        public void ResetGrade()
        {
            if (_startMaxPoints != -1)
            {
               _maxPoints = _startMaxPoints;
               OnMaxPointsChanged?.Invoke();
            }
        }

        public void UpdateGrade(GameGrade grade)
        {
            if (_startMaxPoints != -1)
            {
               _maxPoints = (int)(grade.CurrentGradePercent * _startMaxPoints);
               OnMaxPointsChanged?.Invoke();
            }
        }

        private void AddPoints(int points)
        {
            _curPoints += points;
            if (_curPoints > _maxPoints)
            {
                _curPoints = _maxPoints;
            }
            OnPointsChanged?.Invoke();

            if (_curPoints <= _minPoints)
            {
                _curPoints = _minPoints;
                OnPointsEnds?.Invoke();
            }
        }

        private void OnDestroy()
        {
            OnPointsChanged = null;
            OnPointsEnds = null;
        }
    }
}
