using System;
using UnityEngine;

namespace Core.Units
{
    public abstract class HPDamage : MonoBehaviour, IGradable
    {
        public int DamagePerAttack => _damagePerAttack;

        public Action OnAttack;

        [SerializeField] protected LayerMask _layerMask;
        [SerializeField] protected int _damagePerAttack = 10;

        private int _startDamagePerAttack = -1;


        private void Awake()
        {
            _startDamagePerAttack = _damagePerAttack;
        }

        public void SetDamagePerAttack(int value)
        {
            _damagePerAttack = value;
        }

        protected bool TryAttack(HPControl hp)
        {
            if (hp == null) { return false; }

            hp.AddDamage(_damagePerAttack);
            OnAttack?.Invoke();
            return true;
        }

        public void Reset()
        {

        }

        public void ResetGrade()
        {
            if (_startDamagePerAttack != -1)
            {
               _damagePerAttack = _startDamagePerAttack;
            }
        }

        public void UpdateGrade(GameGrade grade)
        {
            if (_startDamagePerAttack != -1)
            {
               _damagePerAttack = (int)(grade.CurrentGradePercent * _startDamagePerAttack);
            }
        }
    }
}
