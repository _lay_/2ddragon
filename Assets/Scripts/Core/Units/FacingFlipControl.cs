﻿using UnityEngine;

namespace Core.Units
{
    public class FacingFlipControl
    {
        public Facing CurrentFacing => _facing;

        private Transform _ts;
        private Vector3 _objectScale;
        private Facing _facing = Facing.Right;


        public FacingFlipControl(Transform ts)
        {
            _ts = ts;
            _objectScale = _ts.localScale;
        }

        public void SetFacing(Facing nf)
        {
            if (nf != _facing)
            {
                FlipTo(nf);
            }
        }

        private void FlipTo(Facing newFacing)
        {
            Vector3 flip = _ts.localScale;
            flip.x = _objectScale.x * (int)newFacing;
            _ts.localScale = flip;

            _facing = newFacing;
        }
    }    
}
