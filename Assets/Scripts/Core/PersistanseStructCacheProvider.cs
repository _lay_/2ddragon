using System;
using UnityEngine;

namespace Core
{
    public abstract class PersistanseStructCacheProvider<T> where T : struct
    {
        public event Action onDataChanged;

        protected abstract string _name { get; }
        protected T _data;

        private bool _isDirty = true;


        public void Save(T data)
        {
            Validate();

            _data = data;
            PersistanseProvider.Save<T>(_name, _data);
            Debug.Log($"[PersistanseStructCacheProvider] {_name} saved");

            UpdateData();
            _isDirty = false;
            onDataChanged?.Invoke();
        }

        public T Load()
        {
            if (_isDirty)
            {
                if (PersistanseProvider.Exists(_name))
                {
                    _data = PersistanseProvider.Load<T>(_name);
                    Debug.Log($"[PersistanseStructCacheProvider] {_name} loaded");
                }
                else
                {
                    _data = new T();
                    Debug.Log($"[PersistanseStructCacheProvider] Default {_name} loaded");
                }
                _isDirty = false;
                onDataChanged?.Invoke();
            }

            UpdateData();
            return _data;
        }

        protected virtual void Validate() { }
        protected virtual void UpdateData() { }
    }
}