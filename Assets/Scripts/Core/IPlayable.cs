using System;

namespace Core
{
    public interface IPlayable
    {
        public bool IsPlaying { get; }
        public UnityEngine.GameObject gameObject { get; }

        public event Action OnPlay;
        public event Action OnStop;

        public void Play();
        public void Stop();
    }
}