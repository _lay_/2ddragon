﻿using System;
using UnityEngine;
using Core;
using Core.Units;
using Core.UI;
using Game;

namespace Units
{
    public class Enemy : UnitBase, IPlayable
    {
        enum EnemyState { Patrol, WaitPatrol, Chase, WaitChase, Died, None }
        enum AnimParams { Walk, Died, Attack, Hit }

        public bool IsPlaying => _isPlaying;

        public event Action OnPlay;
        public event Action OnStop;

        [SerializeField] private EnemyVision _vision;

        [SerializeField] private Facing _startPatrolDir = Facing.Right;
        [SerializeField] private float _walkDistance = 4f;
        [SerializeField] private float _walkSpeed = 1f;
        [SerializeField] private float _chaseSpeed = 1.5f;
        [SerializeField] private float _chaseOffset = 1f;
        [SerializeField] private float _timeToWait = 5f;
        [SerializeField] private float _timeToWaitChase = 3f;
        [SerializeField] private float _attackDist = 1f;
        [SerializeField] private float _attackDelay = 3f;
        [SerializeField] private float _dieDestroyDelay = 3.5f;

        [Space]
        [Header("Sounds")]
        [SerializeField] private AudioClip _walkClip;
        [SerializeField] private AudioClip _attackClip;
        [SerializeField] private AudioClip _hitClip;
        [SerializeField] private AudioClip _deadClip;

        [Space()]
        [Header("Debug")]
        [SerializeField] private EnemyState _state = EnemyState.None;
        [SerializeField] private Facing _curMoveDir;
        private Vector2 _leftBoundaryPosition;
        private Vector2 _rightBoundaryPosition;
        private float _curSpeed;
        [SerializeField] private float _waitTimer;
        [SerializeField] private float _chaseTimer;
        [SerializeField] private float _attackTimer;

        private bool _isInPatrolArea => _leftBoundaryPosition.x <= transform.position.x && transform.position.x <= _rightBoundaryPosition.x;
        private bool _isInChaseArea => _leftBoundaryPosition.x - _chaseOffset <= transform.position.x && transform.position.x <= _rightBoundaryPosition.x + _chaseOffset;

        private bool _isPlaying = false;

        private UIHealthProvider _ui = new UIHealthProvider();

        private new void Start()
        {
            
        }

        protected override void Init()
        {
            InitGrades();
            _collider.enabled = true;
            _rb.bodyType = RigidbodyType2D.Dynamic;

            _animator.keepAnimatorControllerStateOnDisable = true;
            
            _curMoveDir = _startPatrolDir;
            _waitTimer = _timeToWait;

            _hpControl.ResetHP();
            _hpControl.OnPointsChanged += OnHPChanged;
            _hpControl.OnPointsEnds += OnHPEnd;

            if (_startPatrolDir == Facing.Left)
            {
                _leftBoundaryPosition = (Vector2)transform.position + Vector2.left * _walkDistance;
                _rightBoundaryPosition = transform.position;
            }
            else
            {
                _leftBoundaryPosition = transform.position;
                _rightBoundaryPosition = (Vector2)transform.position + Vector2.right * _walkDistance;
            }
            
            _vision.SetDirection(_curMoveDir);
            _facingControl.SetFacing(_curMoveDir);
            _state = EnemyState.Patrol;

            // FindObjectOfType<GameScreen>()?.EnemyStatus.SetTarget(_hpControl);
        }
        
        protected override void Deinit()
        {
            _hpControl.OnPointsChanged -= OnHPChanged;
            _hpControl.OnPointsEnds -= OnHPEnd;
        }


        private GameGradeController _grades;
        protected override void InitGrades()
        {
            _grades = GameInstance.Instance.GameGrades;

            _grades.EnemyAttack.OnGradeChanged += OnGradesChanged;
            _grades.EnemyHealth.OnGradeChanged += OnGradesChanged;
        }
        protected override void DeinitGrades()
        {
            if (_grades == null) { return; }

            _grades.EnemyAttack.OnGradeChanged -= OnGradesChanged;
            _grades.EnemyHealth.OnGradeChanged -= OnGradesChanged;
        }

        private void OnGradesChanged()
        {
            _attackControl.UpdateGrade(GameInstance.Instance.GameGrades.EnemyAttack);
            _hpControl.UpdateGrade(GameInstance.Instance.GameGrades.EnemyHealth);
        }

        public void Play()
        {
            if (_isPlaying) { return; }

            OnGradesChanged();
            Init();
            _ui.Attach(_hpControl);
            _isPlaying = true;
            gameObject.SetActive(true);
            OnPlay?.Invoke();
        }
        public void Stop()
        {
            if (!_isPlaying) { return; }

            _ui.Deattach();
            _facingControl.SetFacing(Facing.Right);
            ResetAnims();
            Deinit();
            gameObject.SetActive(false);
            _isPlaying = false;
            OnStop?.Invoke();
        }

        private void Update()
        {
            if (!_isPlaying) { return; }

            if (_state == EnemyState.WaitPatrol)
            {
                WaitPatrol();
            }
            if (_state == EnemyState.WaitChase)
            {
                WaitChase();
            }

            if (_vision.CurrentHitObject != null && _isInChaseArea && _state != EnemyState.Died)
            {
                _waitTimer = _timeToWait;
                ChangeState(EnemyState.Chase);
            }

            if (_attackTimer > 0)
            {
                _attackTimer -= Time.deltaTime;
            }
        }

        private void FixedUpdate()
        {
            if (_state == EnemyState.Chase)
            {
                Chase();
            }
            else if (_state == EnemyState.Patrol)
            {
                Patrol();
            }

            _animator.SetBool(nameof(AnimParams.Walk), _state == EnemyState.Patrol || _state == EnemyState.Chase);
        }

        private void WalkStep()
        {
            _rb.MovePosition((Vector2)transform.position + Vector2.right * (int)_curMoveDir * _curSpeed * Time.fixedDeltaTime);
        }

        private void Patrol()
        {
            _curSpeed = _walkSpeed;

            WalkStep();

            if ((_curMoveDir == Facing.Right && transform.position.x >= _rightBoundaryPosition.x)
            || (_curMoveDir == Facing.Left && transform.position.x <= _leftBoundaryPosition.x)
            )
            {
                ChangeState(EnemyState.WaitPatrol);
            }
        }

        private void WaitPatrol()
        {
            _waitTimer -= Time.deltaTime;
            if (_waitTimer <= 0)
            {
                _waitTimer = _timeToWait;
                FlipDirection();

                ChangeState(EnemyState.Patrol);
            }
        }

        private void WaitChase()
        {
            _chaseTimer -= Time.deltaTime;
            if (_chaseTimer <= 0)
            {
                _chaseTimer = _timeToWaitChase;

                ChangeState(EnemyState.Patrol);
            }
        }

        // gg
        private void Chase()
        {
            float dist = 0;
            bool endOfArea = false;
            bool lostHit = false;
            _curSpeed = _chaseSpeed;
            
            if (_vision.CurrentHitObject != null)
            {
                dist = _vision.CurrentHitObject.transform.position.x - transform.position.x;
                if (Mathf.Abs(_vision.CurrentHitDistance) <= _attackDist && _attackTimer <= 0)
                {
                    _attackTimer = _attackDelay;
                    _animator.SetTrigger(nameof(AnimParams.Attack));
                    PlaySound(_attackClip);
                    _attackControl.TryPointAttack(_vision.CurrentHitObject.transform.position);
                }
            }
            else
            {
                dist = (int)_curMoveDir;
                lostHit = true;
            }

            if (!lostHit)
            {
                if (transform.position.x < _leftBoundaryPosition.x - _chaseOffset)
                {
                    dist = 1;
                    endOfArea = true;
                }
                else if (transform.position.x > _rightBoundaryPosition.x + _chaseOffset)
                {
                    dist = -1;
                    endOfArea = true;
                }
            }
            
            if (dist > 0)
            {
                SetDirection(Facing.Right);
            }
            else
            {
                SetDirection(Facing.Left);
            }

            if (endOfArea || lostHit)
            {
                _chaseTimer = _timeToWaitChase;
                ChangeState(EnemyState.WaitChase);
            }
            else
            {
                WalkStep();
            }
        }

        private void FlipDirection()
        {
            SetDirection(_curMoveDir == Facing.Left ? Facing.Right : Facing.Left);
        }
        private void SetDirection(Facing newDir)
        {
            if (_curMoveDir != newDir)
            {
                _curMoveDir = newDir;
                _facingControl.SetFacing(_curMoveDir);
                _vision.SetDirection(_curMoveDir);
            }
        }

        private void ChangeState(EnemyState newState)
        {
            if (_state != newState)
            {
                _state = newState;
                
                ResetAnims();
            }
        }

        private void ResetAnims()
        {
            _animator.ResetTrigger(nameof(AnimParams.Attack));
            _animator.ResetTrigger(nameof(AnimParams.Hit));
            _animator.SetBool(nameof(AnimParams.Died), false);
        }

        private void OnHPChanged()
        {
            if (_hpControl.IsAlive)
            {
                PlaySound(_hitClip);
                _animator.SetTrigger(nameof(AnimParams.Hit));
            }
        }

        private void OnHPEnd()
        {
            ChangeState(EnemyState.Died);
            _animator.SetBool(nameof(AnimParams.Died), true);

            _rb.bodyType = RigidbodyType2D.Static;
            _collider.enabled = false;

            PlaySound(_deadClip);

            _ui.Deattach();
            GameEvents.OnEnemyKill?.Invoke();
            DelayedAction(_dieDestroyDelay, () => { Stop(); });
        }

        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (Application.isPlaying && this.gameObject.activeSelf)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(_leftBoundaryPosition, _rightBoundaryPosition);
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(_leftBoundaryPosition + Vector2.left * _chaseOffset, _leftBoundaryPosition);
                Gizmos.DrawLine(_rightBoundaryPosition, _rightBoundaryPosition + Vector2.right * _chaseOffset);
            }
#endif
        }    
    }
}
