using Core.Units;
using UnityEngine;

namespace Units
{
    public class TriggerDamageControl : HPDamage
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var hp = other.GetComponent<HPControl>();

            TryAttack(hp);
        }
    }
}
