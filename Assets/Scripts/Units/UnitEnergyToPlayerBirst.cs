using UnityEngine;
using Core.Units;
using Game;
using Effects;

namespace Units
{
    [RequireComponent(typeof(UnitBase))]
    public class UnitEnergyToPlayerBirst : MonoBehaviour
    {
        [SerializeField] private int _energyPerBitst = 10;
        [SerializeField] private GameObject _effectPrefab;
        [SerializeField] private Color _effectColor = Color.gray;

        private UnitBase _unit;

        private void Awake()
        {
            _unit = GetComponent<UnitBase>();
            _unit.OnDied += OnTargetDied;
        }

        private void OnValidate()
        {
            if (_effectPrefab != null && !_effectPrefab.GetComponent<TwoPointsEffect>())
            {
                _effectPrefab = null;
                Debug.LogError("[UnitEnergyToPlayer] Need atach TwoPointsEffect!");
            }
        }

        private void OnTargetDied()
        {
            var target = GameInstance.Instance.GameMode.Player;
            if (target == null) { return; }

            var go = Instantiate(_effectPrefab);
            var effect = go.GetComponent<TwoPointsEffect>();

            effect.SetColor(_effectColor);
            effect.StartEffect(_unit.transform.position, target.transform);

            GameInstance.Instance.GameMode.AddLifeEnergy(_energyPerBitst);
        }

        private void OnDestroy()
        {
            _unit.OnDied -= OnTargetDied;
        }
    }
}
