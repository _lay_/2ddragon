﻿using Core.Units;
using UnityEngine;

namespace Units
{
    public class EnemyVision : MonoBehaviour
    {
        public GameObject CurrentHitObject => _currentHitObject;
        public float CurrentHitDistance => _currentHitDistance;

        [SerializeField] private float _circleRadius;
        [SerializeField] private float _maxDistance;
        [SerializeField] private LayerMask _layerMask;
        [Space()]
        [Header("Debug")]
        [SerializeField] private GameObject _currentHitObject;
        [SerializeField] private float _currentHitDistance;

        private Vector2 _origin;
        [SerializeField] private Vector2 _direction;


        private void Start()
        {
            _currentHitDistance = _maxDistance;
        }

        public void SetDirection(Facing newDir)
        {
            SetDirectionTo(newDir == Facing.Left ? Vector2.left : Vector2.right);
        }
        
        private void SetDirectionTo(Vector2 newDir)
        {
            if (_direction != newDir)
            {
                _direction = newDir;
            }
        }

        private void Update()
        {
            _origin = transform.position;
            
            RaycastHit2D hit = Physics2D.CircleCast(_origin, _circleRadius, _direction, _maxDistance, _layerMask);

            if (hit && hit.transform.gameObject.GetComponent<Player>())
            {
                _currentHitObject = hit.transform.gameObject;
                _currentHitDistance = hit.distance;

            }
            else
            {
                _currentHitObject = null;
                _currentHitDistance = _maxDistance;
            }
        }

        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (Application.isPlaying && this.gameObject.activeSelf)
            {
                Vector2 hit = _origin + _direction * _currentHitDistance;

                Gizmos.color = Color.blue;
                Gizmos.DrawLine(_origin, hit);
                Gizmos.DrawWireSphere(hit, _circleRadius);
            }
#endif
        }
    }
}
