﻿using System;
using UnityEngine;
using Core.Units;
using Core.UI;
using Core.Input;
using Map.Env;
using Env;
using Game;

namespace Units
{
    public class Player : UnitBase
    {
        enum JumpState { Ground, Jump, DoubleJump, DownJump }
        enum AnimParams { SpeedX, Died, Attack, Hit }

        public bool CanLeft = true;

        [Space]
        [SerializeField] private float _horSpeed = 4f;
        [SerializeField] private float _climbSpeed = 4f;
        [SerializeField] private float _jumpForce = 320f;
        [SerializeField] private float _interactRadius = 1.5f;
        [SerializeField] private float _facingDeadZone = 0.1f;
        [SerializeField] private float _dieEndDelay = 3.5f;

        [Space]
        [Header("Sounds")]
        [SerializeField] private AudioClip _walkClip;
        [SerializeField] private AudioClip _jumpClip;
        [SerializeField] private AudioClip _jumpDoubleClip;
        [SerializeField] private AudioClip _attackClip;
        [SerializeField] private AudioClip _hitClip;
        [SerializeField] private AudioClip _deadClip;

        private float _horizontal;
        private float _vertical;
        // private float _prevHorizontal;
        private bool _isNeedJumpUp = false;
        private bool _isNeedJumpDown = false;
        private float _downTargetY;
        private JumpState _jumpState = JumpState.Ground;

        private bool _isClimbing = false;
        private bool _onLadder = false;

        private bool _canJumpUp => !_isNeedJumpUp
                                && (_jumpState == JumpState.Ground || _jumpState == JumpState.Jump);
        private bool _canJumpDown => !_isNeedJumpDown
                                  && _jumpState == JumpState.Ground;
        private bool _isDied => _animator.GetBool(nameof(AnimParams.Died));

        private UIHealthProvider _ui = new UIHealthProvider();


#region Input

        private float _inputHorizontal
        {
            get
            {
                float axis = Input.GetAxis("Horizontal");

                return axis == 0 ? _mHorizontal : axis;
            }
        }
        private float _inputVertical
        {
            get
            {
                float axis = Input.GetAxis("Vertical");
                
                return axis == 0 ? _mVertical : axis;
            }
        }

        private bool _inputJump => Input.GetKeyDown(KeyCode.W)
                                || Input.GetKeyDown(KeyCode.UpArrow)
                                || _mJumpDown;
        private bool _inputJumpFall => Input.GetKeyDown(KeyCode.S)
                                    || Input.GetKeyDown(KeyCode.DownArrow)
                                    || _mJumpFallDown;
        private bool _inputAttack => Input.GetKeyDown(KeyCode.Space)
                                  || _mAttackDown;



        private MobileInput _mInput;

        private float _mHorizontal => _mInput != null ?  _mInput.joystickHorizontal : 0;
        private float _mVertical => _mInput != null ?  _mInput.joystickVertical : 0;

        private bool _mAttackDown => _mInput != null && _mInput.GetButtonDown(MobileBtnType.Attack);
        private bool _mJumpDown => _mInput != null && _mInput.GetButtonDown(MobileBtnType.Jump);
        private bool _mJumpFallDown => _mInput != null && _mInput.GetButtonDown(MobileBtnType.JumpFall);

#endregion


        protected override void Init()
        {
            OnGradesChanged();

            _hpControl.ResetHP();
            _hpControl.OnPointsChanged += OnHPChanged;
            _hpControl.OnPointsEnds += OnHPEnd;

            _ui.Attach(_hpControl);

            ResetAnims();

            _mInput = FindObjectOfType<MobileInput>();
        }
        
        protected override void Deinit()
        {
            _hpControl.OnPointsChanged -= OnHPChanged;
            _hpControl.OnPointsEnds -= OnHPEnd;

            _ui?.Deattach();
        }

        private GameGradeController _grades;
        protected override void InitGrades()
        {
            _grades = GameInstance.Instance.GameGrades;

            _grades.PlayerAttack.OnGradeChanged += OnGradesChanged;
            _grades.PlayerHealth.OnGradeChanged += OnGradesChanged;
        }
        protected override void DeinitGrades()
        {
            if (_grades == null) { return; }

            _grades.PlayerAttack.OnGradeChanged -= OnGradesChanged;
            _grades.PlayerHealth.OnGradeChanged -= OnGradesChanged;
        }

        private void OnGradesChanged()
        {
            var gr = GameInstance.Instance.GameGrades;

            Debug.Log($"[Player] OnGradesChanged ({gr.PlayerAttack.CurrentGrade} {gr.PlayerHealth.CurrentGrade})");
            _attackControl.UpdateGrade(gr.PlayerAttack);
            _hpControl.UpdateGrade(gr.PlayerHealth);
        }

        private void Update()
        {
            if (_isDied)
            {
                _horizontal = 0;
                _animator.SetFloat(nameof(AnimParams.SpeedX), 0);
                // _collider.isTrigger = true;
                return;
            }

            _horizontal = _inputHorizontal;
            _vertical = _inputVertical;
            
            if (_onLadder && Math.Abs(_vertical) > 0f)
            {
                _isClimbing = true;
            }

            _animator.SetFloat(nameof(AnimParams.SpeedX), Mathf.Abs(_horizontal));

            if (_inputJump && _canJumpUp && !_isClimbing)
            {
                _isNeedJumpUp = true;
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                var cols = Physics2D.CircleCastAll(_collider.transform.position, _interactRadius, Vector2.zero);
                Array.ForEach(cols, itm => itm.collider.gameObject.GetComponent<Activatable>()?.Activate());
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                var cols = Physics2D.CircleCastAll(_collider.transform.position, _interactRadius, Vector2.zero);
                Array.ForEach(cols, itm => itm.collider.gameObject.GetComponent<Activatable>()?.Deactivate());
            }

            if (_inputAttack)
            {
                _animator.SetTrigger(nameof(AnimParams.Attack));

                var p = transform.position + Vector3.right * (int) _facingControl.CurrentFacing * 0.3f;
                _attackControl.TryPointAttack(p);

                PlaySound(_attackClip);
            }

            if (_inputJumpFall && _canJumpDown)
            {
                _isNeedJumpDown = true;
            }

            if (transform.position.y < _downTargetY)
            {
                _collider.enabled = true;
            }
        }

        private void FixedUpdate()
        {
            if (!CanLeft && _horizontal < 0)
            {
                _horizontal = 0;
            }

            _rb.velocity = new Vector2(_horizontal * _horSpeed, _rb.velocity.y);

            if (_isClimbing)
            {
                _rb.gravityScale = 0f;
                _rb.velocity = new Vector2(_rb.velocity.x, _vertical * _climbSpeed);

                if (_isNeedJumpDown)
                {
                    _isNeedJumpDown = false;

                    _downTargetY = transform.position.y - .9f;
                    _collider.enabled = false;
                }
            }
            else
            {
                _rb.gravityScale = 1f;
            }

            if (_isNeedJumpUp)
            {
                _isNeedJumpUp = false;
                _jumpState =
                    _jumpState == JumpState.Jump ? JumpState.DoubleJump : JumpState.Jump;

                if (_jumpState == JumpState.DoubleJump)
                {
                    _rb.velocity = new Vector2(_rb.velocity.x, 0);
                }
                _rb.AddForce(new Vector2(0, _jumpForce));

                PlaySound(_jumpState == JumpState.Jump ? _jumpClip : _jumpDoubleClip);
            }

            if (_isNeedJumpDown && !_isClimbing)
            {
                _isNeedJumpDown = false;
                _jumpState = JumpState.DownJump;
                _downTargetY = transform.position.y - .9f;
                
                _rb.AddForce(new Vector2(0, _jumpForce * .5f));
                _collider.enabled = false;
                PlaySound(_jumpClip);
            }

            if (Mathf.Abs(_horizontal) > _facingDeadZone)
            {
                _facingControl.SetFacing(_horizontal < -_facingDeadZone
                    ? Facing.Left
                    : _horizontal > _facingDeadZone
                        ? Facing.Right : _facingControl.CurrentFacing
                );
            }
        }

        private void ResetAnims()
        {
            _animator.SetFloat(nameof(AnimParams.SpeedX), 0f);
            _animator.SetBool(nameof(AnimParams.Died), false);
            _animator.ResetTrigger(nameof(AnimParams.Attack));
            _animator.ResetTrigger(nameof(AnimParams.Hit));
        }

        private void OnHPChanged()
        {
            if (_hpControl.IsAlive)
            {
                PlaySound(_hitClip);
                _animator.SetTrigger(nameof(AnimParams.Hit));
            }
        }

        private void OnHPEnd()
        {
            PlaySound(_deadClip);
            _animator.SetBool(nameof(AnimParams.Died), true);

            _ui.Deattach();

            DelayedAction(_dieEndDelay, () =>{  });
        }

        private bool IsGrounded()
        {
            return Physics2D.BoxCast(_collider.bounds.center + Vector3.down * (_collider.bounds.extents.y + .1f), new Vector2(_collider.bounds.size.x, .1f), 0, Vector2.down, .2f);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (Physics2D.CircleCast(transform.position, 0.1f, Vector2.down))
            if (IsGrounded())
            {
                _jumpState = JumpState.Ground;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<LadderCell>())
            {
                _onLadder = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.GetComponent<LadderCell>())
            {
                _onLadder = false;
                _isClimbing = false;
            }
        }
    }
}
