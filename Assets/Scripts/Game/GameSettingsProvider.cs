using UnityEngine;
using Core;

namespace Game
{
    public class GameSettingsProvider : PersistanseStructCacheProvider<GameSettings>
    {
        protected override string _name => nameof(GameSettings);

        protected override void Validate()
        {
            if (_data.SoundsVolume < 0 || _data.SoundsVolume > 1)
            {
                throw new System.ArgumentOutOfRangeException("soundsVolume");
            }
        }

        protected override void UpdateData()
        {
            AudioListener.volume = _data.SoundsEnabled ? _data.SoundsVolume : 0f;
        }
    }
}
