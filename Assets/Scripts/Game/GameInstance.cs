using System;
using System.Collections;
using UnityEngine;
using Core;
using Core.UI;
using UI;

namespace Game
{
    public class GameInstance : SingletonMonoBehaviour<GameInstance>
    {
        public GameMode GameMode => _gameMode;
        public GameScreen GameScreen => _gameScreen;
        public UIHealthScreen HealthScreen => _healthScreen;
        public GameGradeController GameGrades => _gameGrades;

        private GameMode _gameMode;
        private GameScreen _gameScreen;
        private UIHealthScreen _healthScreen;
        private GameGradeController _gameGrades;


        private new void Awake()
        {
            _dontDestroyOnLoad = false;
            _dontDestroyOnLoad = false;

            GameEvents.OnLevelEnd += DeinitAll;

            GameDataProvider.ReloadAll();
        }

        private void Start()
        {
            GameDataProvider.GameSettings.Load();

            _gameMode = FindObjectOfType<GameMode>();
            _gameScreen = FindObjectOfType<GameScreen>();
            _healthScreen = FindObjectOfType<UIHealthScreen>();
            _gameGrades = FindObjectOfType<GameGradeController>();

            InitAll();

            _gameMode.StartLevel();
        }

        private void InitAll()
        {
            _gameGrades.Init();
            _gameMode.Init();

            _gameScreen.Init();
            _healthScreen.Init();
        }

        private void DeinitAll()
        {
            _gameGrades?.Deinit();
            _gameMode?.Deinit();

            _gameScreen?.Deinit();
            _healthScreen?.Deinit();
        }

        public void DelayedAction(float delay, Action action)
        {
            StartCoroutine(DelayedRoutine(delay, action));
        }

        private IEnumerator DelayedRoutine(float delay, Action action)
        {
            if (action != null)
            {
                yield return new WaitForSeconds(delay);
                action.Invoke();
            }
        }

        private new void OnDestroy()
        {
            StopAllCoroutines();
            DeinitAll();
            base.OnDestroy();
        }
    }
}