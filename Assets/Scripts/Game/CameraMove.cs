using UnityEngine;
using Units;

namespace Game
{
    [RequireComponent(typeof(Camera))]
    public class CameraMove : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)] private float _leftBorder = 0.04f;
        [SerializeField, Range(0f, 1f)] private float _rightBorder = 0.5f;
        [SerializeField] private float _smoothTime = 0.5f;
        [SerializeField] private bool _useFixedUpdate = false;
        

        private Player _target;
        private Camera _camera;

        private bool _needRight = false;
        private Vector3 _velocity = Vector3.zero;


        private void Start()
        {
            _camera = GetComponent<Camera>();
            _target = FindObjectOfType<Player>();
        }

        public void SetTarget(Player target)
        {
            _target = target;
        }

        private void Update()
        {
            if (_useFixedUpdate || !_target)  { return; };
            UpdatePosition();
        }

        private void FixedUpdate()
        {
            if (!_target) { return; }

            var p = _camera.WorldToViewportPoint(_target.transform.position);

            _needRight = p.x > _rightBorder;
            _target.CanLeft = p.x > _leftBorder;

            if (!_useFixedUpdate) return;
            UpdatePosition();
        }

        private void UpdatePosition()
        {
            if (_needRight)
            {
                var targetPosition = new Vector3(_target.transform.position.x, _camera.transform.position.y, _camera.transform.position.z);
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref _velocity, _smoothTime);
            }
        }
    }
}
