namespace Game
{
    public static class GameDataProvider
    {
        public static GameSaveProvider GameSave => _gameSave;
        public static GameSettingsProvider GameSettings => _gameSettings;

        private static GameSaveProvider _gameSave = new GameSaveProvider();
        private static GameSettingsProvider _gameSettings = new GameSettingsProvider();

        
        public static void ReloadAll()
        {
            _gameSettings.Load();
            _gameSave.Load();
        }
    }
}