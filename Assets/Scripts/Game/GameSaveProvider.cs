using Core;

namespace Game
{
    public class GameSaveProvider : PersistanseStructCacheProvider<GameSave>
    {
        protected override string _name => nameof(GameSave);

        protected override void Validate()
        {
            if (_data.MaxDistance < 0)
            {
                throw new System.ArgumentOutOfRangeException("maxDistance");
            }
            if (_data.EnemyKills < 0)
            {
                throw new System.ArgumentOutOfRangeException("enemyKills");
            }
            if (_data.Deads < 0)
            {
                throw new System.ArgumentOutOfRangeException("deads");
            }
            if (_data.LifeEnergy < 0)
            {
                throw new System.ArgumentOutOfRangeException("lifeEnergy");
            }
            if (_data.PlayerAttackGrade < 0)
            {
                throw new System.ArgumentOutOfRangeException("playerAttackGrade");
            }
            if (_data.PlayerHealthGrade < 0)
            {
                throw new System.ArgumentOutOfRangeException("playerHealthGrade");
            }
        }

        protected override void UpdateData()
        {
            
        }
    }
}