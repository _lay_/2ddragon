
namespace Game
{
    [System.Serializable]
    public struct GameSettings
    {
        public bool SoundsEnabled;
        public float SoundsVolume;

        public GameSettings(bool soundsEnabled=true, float soundsVolume=1f)
        {
            SoundsEnabled = soundsEnabled;
            SoundsVolume = soundsVolume;
        }
    }
}