using UnityEngine;
using Core;
using Map;
using Units;
using UI;

namespace Game
{
    public class GameMode : MonoBehaviour
    {
        public int Distance => _pastDistance;
        public int LifeEnergy => _lifeEnergy;
        public Player Player => _player;

        [SerializeField] private LevelGenerator _levelGenerator;
        [SerializeField] private GameObject _playerPrefab;

        private Player _player;
        private int _startDistance = 0;
        private int _pastDistance;
        private int _lifeEnergy;
        private int _sessionEnemyKills;
        private bool _playerDead;

        private GameSaveProvider _gameSave => GameDataProvider.GameSave;


        public void Init()
        {
            _gameSave.onDataChanged += OnGameSaveUpdated;
            GameEvents.OnEnemyKill += OnEnemyKill;
            GameEvents.OnLevelEnd += SaveData;
            GameEvents.OnGameExit += SaveData;
            GameEvents.OnPauseChanged += OnPauseChanged;
        }

        public void Deinit()
        {
            if (_player != null)
            {
                _player.OnDied -= OnPlayerDeath;
            }
            _gameSave.onDataChanged -= OnGameSaveUpdated;
            GameEvents.OnEnemyKill -= OnEnemyKill;
            GameEvents.OnLevelEnd -= SaveData;
            GameEvents.OnGameExit -= SaveData;
            GameEvents.OnPauseChanged -= OnPauseChanged;
        }

        private void OnValidate()
        {
            if (_playerPrefab != null && _playerPrefab.GetComponent<Player>() == null)
            {
                Debug.LogError($"[LevelGenerator] Cant attach this to {nameof(_playerPrefab)}, need component {typeof(Player).Name}!");
                _playerPrefab = null;
            }
        }
        
        public void ResetPlayer()
        {
            var players = FindObjectsOfType<Player>();
            System.Array.ForEach(players, p => Destroy(p.gameObject));

            var go = Instantiate(_playerPrefab);
            var camm = FindObjectOfType<Game.CameraMove>();
            var p = go.GetComponent<Player>();
            camm?.SetTarget(p);

            p.OnDied += OnPlayerDeath;
            _player = p;
        }

        public void StartLevel()
        {
            _pastDistance = 0;
            _lifeEnergy = 0;
            _sessionEnemyKills = 0;
            _playerDead = false;

            GameInstance.Instance.GameGrades.ResetGrades();
            LoadData();
            ResetPlayer();
            _levelGenerator.StartLevel();
            _startDistance = (int)_player.transform.position.x;
            GameEvents.OnLevelStarted?.Invoke();
        }

        public void AddLifeEnergy(int value)
        {
            _lifeEnergy += value;
        }

        private void FixedUpdate()
        {
            UpdateDistance();
        }

        private void UpdateDistance()
        {
            if (_player == null) { return; }

            int newDist = (int)_player.transform.position.x - _startDistance;

            if (_pastDistance < newDist)
            {
                _pastDistance = newDist;
            }
        }

        // [ContextMenu("SaveData")]
        private void SaveData()
        {
            _gameSave.onDataChanged -= OnGameSaveUpdated;
            var save = _gameSave.Load();
            var gr = GameInstance.Instance.GameGrades;

            save.MaxDistance = Mathf.Max(save.MaxDistance, _pastDistance);
            save.EnemyKills += _sessionEnemyKills;
            save.Deads = _playerDead ? save.Deads+1 : save.Deads;
            save.LifeEnergy = _lifeEnergy;
            save.PlayerAttackGrade = gr.PlayerAttack.CurrentGrade;
            save.PlayerHealthGrade = gr.PlayerHealth.CurrentGrade;

            _playerDead = false;

            _gameSave.Save(save);
            _gameSave.onDataChanged += OnGameSaveUpdated;
            Debug.Log("[GameMode] Game data saved");
        }

        private void LoadData()
        {
            var save = _gameSave.Load();

            _lifeEnergy = save.LifeEnergy;
            GameInstance.Instance.GameGrades.LoadGrades();
            Debug.Log("[GameMode] Game data loaded");
        }

        private void OnGameSaveUpdated()
        {
            LoadData();
        }

        private void OnPlayerDeath()
        {
            _playerDead = true;
            SaveData();
            GameInstance.Instance.GameScreen.ShowDeadDialog();
        }

        private void OnPauseChanged(bool paused)
        {
            var save = _gameSave.Load();

            if (paused)
            {                
                save.LifeEnergy = _lifeEnergy;
                _gameSave.Save(save);
            }
            else
            {
                _lifeEnergy = save.LifeEnergy;
            }
        }

        private void OnEnemyKill()
        {
            _sessionEnemyKills++;
        }
    }
}