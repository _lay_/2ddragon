using UnityEngine;
using Core;

namespace Game
{
    public class GameGradeController : MonoBehaviour
    {
        [System.Serializable]
        private sealed class GameGradesSettings
        {
            [Min(0f)]public float PlayerAttack = .08f;
            [Min(0f)]public float PlayerHealth = .09f;
            [Min(0f)]public float EnemyAttack = .1f;
            [Min(0f)]public float EnemyHealth = .1f;
        }

        public GameGrade PlayerAttack => _playerAttack;
        public GameGrade PlayerHealth => _playerHealth;
        public GameGrade EnemyAttack => _enemyAttack;
        public GameGrade EnemyHealth => _enemyHealth;

        [SerializeField] private int _startGameGrade = 0;
        [SerializeField] private GameGradesSettings _gameGradesSettings;

        private GameGrade _playerAttack;
        private GameGrade _playerHealth;
        private GameGrade _enemyAttack;
        private GameGrade _enemyHealth;


        public void Init()
        {
            LoadSettings();
            InitGrades();
        }

        public void Deinit()
        {
            
        }

        public void InitGrades()
        {
            _playerAttack = new GameGrade(_gameGradesSettings.PlayerAttack);
            _playerHealth = new GameGrade(_gameGradesSettings.PlayerHealth);
            _enemyAttack = new GameGrade(_gameGradesSettings.EnemyAttack);
            _enemyHealth = new GameGrade(_gameGradesSettings.EnemyHealth);
        }

        public void ResetGrades()
        {
            _playerAttack.Reset();
            _playerHealth.Reset();
            _enemyAttack.Reset();
            _enemyHealth.Reset();

            LoadGrades();

            _enemyAttack.SetUpgrade(_startGameGrade);
            _enemyHealth.SetUpgrade(_startGameGrade);
        }

        public void LoadGrades()
        {
            var save = GameDataProvider.GameSave.Load();

            _playerAttack.SetUpgrade(save.PlayerAttackGrade);
            _playerHealth.SetUpgrade(save.PlayerHealthGrade);
        }

        [ContextMenu("Save settings")]
        public void SaveSettings()
        {
            PersistanseProvider.Save<GameGradeController>(nameof(GameGradesSettings), this);
            Debug.Log("[GameGradeController] Settings saved");
        }

        [ContextMenu("Load settings")]
        public void LoadSettings()
        {
            if (PersistanseProvider.LoadOverride<GameGradeController>(nameof(GameGradesSettings), this))
            {
                Debug.Log("[GameGradeController] Settings loaded");
            }
        }
    }
}