using UnityEngine;
using Core.Units;
using Units;

namespace Game
{
    [RequireComponent(typeof(Collider2D))]
    public class DeadZone : MonoBehaviour
    {
        private Collider2D _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider2D>();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            var p = other.gameObject.GetComponent<Player>();
            if (p != null)
            {
                var hp = p.GetComponent<HPControl>();

                hp.AddDamage(hp.CurPoints);
            }
        }
    }
}

