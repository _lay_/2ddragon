namespace Game
{
    [System.Serializable]
    public struct GameSave
    {
        public int MaxDistance;
        public int EnemyKills;
        public int Deads;
        public int LifeEnergy;
        public int PlayerAttackGrade;
        public int PlayerHealthGrade;

        public GameSave(int maxDistance=0, int enemyKills=0, int deads=0, int lifeEnergy=0, int playerAttackGrade=0, int playerHealthGrade=0)
        {
            MaxDistance = maxDistance;
            EnemyKills = enemyKills;
            Deads = deads;
            LifeEnergy = lifeEnergy;
            PlayerAttackGrade = playerAttackGrade;
            PlayerHealthGrade = playerHealthGrade;
        }
    }
}